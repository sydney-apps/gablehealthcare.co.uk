<?php

/* 
 * @author  Sydney Mlambo
 * 
 */

/*
 * Resources
 * 
 */
define('HOME_PAGE', 'pages/home.php');
define ('ERROR_PAGE', 'pages/404.php');
define('NAV__FILE', $_SERVER['DOCUMENT_ROOT'].'/xml/nav.xml');
define('CONTACT_FORM', $_SERVER['DOCUMENT_ROOT'].'/xml/forms/contact.xml');
define('APPLICATION_FORM_DIR', $_SERVER['DOCUMENT_ROOT'].'/media/applicationforms/');
define('APPLICATION_F0RM_TEMPLATE', $_SERVER['DOCUMENT_ROOT'].'/media/templates/application/applicationform.html');
define('CSS_PATH_FOR_APPLICATION_TEMPLATE', $_SERVER['DOCUMENT_ROOT'].'/media/templates/application/css');
define('APPLICATION_FORM', $_SERVER['DOCUMENT_ROOT'].'/xml/forms/application.xml');

define('WEBMAIL_CONTACT_FORM', 'Webmail Contact Form');
define('APPLICATION_RECIPIENT_EMAIL', 'info@gablehealthcare.co.uk');
// define('APPLICATION_RECIPIENT_EMAIL', 'info@sydneyapps.co.uk');

define('DOWNLOADS', 'downloads');
define('APPLICATION_FORM_PDF', '/downloads/application-form.pdf');
define('SERVICES_BROCHURE_PDF', '/downloads/services-brochure.pdf');

/**** REGEX STRINGS *****/

/**
 *     Input validation types
 */
define('EMAIL', 'E-mail');
define('UK_PHONE', 'Phone');


