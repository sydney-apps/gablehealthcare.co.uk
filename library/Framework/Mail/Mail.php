<?php

/**
 * Description of EMail
 *
 * @author Sydney
 */
class Mail {
    //put your code here
    
    /**
     * 
     * @param type $filename 
     * @param type $mailto
     * @param type $from_mail
     * @param type $from_name
     * @param type $replyto
     * @param type $subject
     * @param type $message
     * @return boolean
     */
    function MailAttachment($filename, $mailto, $from_mail, $from_name, $replyto, $subject, $message) {
        
        $file = $filename;
        $file_size = filesize($file);
        $handle = fopen($file, "r");
        $content = fread($handle, $file_size);
        fclose($handle);
        $content = chunk_split(base64_encode($content));
        $uid = md5(uniqid(time()));
        $header = "From: ".$from_name." <".$from_mail.">\r\n";
        $header .= "Reply-To: ".$replyto."\r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
        $header .= "This is a multi-part message in MIME format.\r\n";
        $header .= "--".$uid."\r\n";
        $header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
        $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $header .= $message."\r\n\r\n";
        $header .= "--".$uid."\r\n";
        $header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
        $header .= "Content-Transfer-Encoding: base64\r\n";
        $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
        $header .= $content."\r\n\r\n";
        $header .= "--".$uid."--";
       
        if (mail($mailto, $subject, "", $header)) {
            return true; // or use booleans here
        } else {
            return false;
        }
        
    }
   
}
