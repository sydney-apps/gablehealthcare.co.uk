<?php
include_once 'utilities/library.php';
include_once 'library/Framework/CreatePDF/ApplicationFormPDF.php';
include_once 'library/Framework/Mail/Mail.php';
include_once 'library/Framework/PHPMailer/PHPMailerClass.php';

class CreateApplicationForm{
    /**
     *  Fields
     */
    //<editor-fold>
    private $dom ;
    private $path = '';
    private $isvalidmail = false;
    private $message = '';
    private $MESSAGE_SEND = 'Message has been sent';
    private $MESSAGE_ERROR = 'An error has occurred.';
	private $MESSAGE_TITLE = 'Online Application Form';
    private $sendstatus = '';
    private $error = false;
    private $HtmlForm;
    
    private $FormXMLDocument;
    private $FormFields;
    
    //</editor-fold>
   
    public function __construct() {
        
        $this->MESSAGE_TITLE = 'Online application form';
        $this->FormXMLDocument = new ContactFormReader(APPLICATION_FORM);
        if(isset($_REQUEST['issend'])){
            $this->isvalidmail = true;
            $this->ProcessForm();
        }
        $this->HtmlForm = $this->FormXMLDocument->SaveXML();
    }

    public function ProcessForm(){
        
        $this->FormFields = new FormElementTypeProcessor($this->FormXMLDocument->GetOrderedFieldNames());
        $ElementGroups = $this->FormXMLDocument->GetListOfElementsByType();
        $this->FormFields->ValidateFields($ElementGroups);
        ($this->FormFields->IsMessageValid()) ? $this->SendPHPMailerMessage() : $this->SetErrorMessage();
        
    }
    /**
     *  :: Validation Methods ::
     */
    //<editor-fold>
    /**
     * 
     * @param string $param name of input
     * @param string $default placeholder value
     * @param string $required is it optional or required
     * @return boolean
     */
 
    //</editor-fold>
    private function ComposeMessage ($name){
        $this->message = "\r\n Hi";
        $this->message .= "\r\n";
        $this->message .= "May you find attached my attached my CV. \r\n";
        $this->message .= "Thanks\r\n";
        $this->message .= $name;
        return nl2br($this->message);
    }
    
    public function ShowFormTitle() {
        echo  $this->MESSAGE_TITLE;
    }

	public function ShowError(){

		if($this->error){
			echo $this->MESSAGE_ERROR;
		}

	}
    
    public function DisplayForm(){
        echo $this->HtmlForm;
    }
    
    public function SendPHPMailerMessage(){
        
        $from = $this->FormFields->GetValuesCollection()->getItem('--email--');
        $from_name = $this->FormFields->GetValuesCollection()->getItem('--name--').' '.$this->FormFields->GetValuesCollection()->getItem('--last-name--');
        $sub = $this->MESSAGE_TITLE;
        mail('info@sydneyapps.co.uk', 'TEst', 'dude');
        
        $MessageBody = $this->FormFields->ComposeMessage();
        $PDFFile = new ApplicationFormPDF();
        $htmlOutput = $PDFFile->GeneratePDF(CSS_PATH_FOR_APPLICATION_TEMPLATE, APPLICATION_F0RM_TEMPLATE, $this->FormFields->GetValuesCollection());
        $fileName = APPLICATION_FORM_DIR.$from_name.str_replace('.', '',microtime()).'.pdf';
        $result = file_put_contents($fileName, $htmlOutput);
        if($result){
            
            $Mail = new PHPMailerClass();
            if ($Mail->send($from, APPLICATION_RECIPIENT_EMAIL, $from, $fileName, $this->ComposeMessage($from_name))){
                $this->sendstatus = $this->MESSAGE_SEND;
            }else{
                $this->SetErrorMessage();
            }
            
        }else{
            $this->SetErrorMessage();
        }
}
    
    public function SendMailMessage(){        
        if(file_put_contents($fileName, $htmlOutput)){
            $Mail = new Mail();
            if ($Mail->MailAttachment($filename, APPLICATION_RECIPIENT_EMAIL, $from, $from_name, $replyto, $sub, $this->ComposeMessage($from_name))){
                $this->sendstatus = $this->MESSAGE_SEND;
            }else{
                $this->SetErrorMessage();
            }
        }
        
    }
    
    private function SetErrorMessage(){
        $this->error = true;
    }
    
}

