<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Collection
 *
 * @author Sydney
 */
class Collection {
    
    public $items = array();
    /**
     * 
     * @param type $obj
     * @param type $key
     * @throws KeyHasUseException
     */
    public function addItem($obj, $key) {
        if ($key == null) {
            throw new KeyHasUseException("Set a $key to use.");
        }else {
            $this->items[$key] = $obj;
        }
    }

    /**
     * 
     * @param type $key
     * @throws KeyInvalidException
     */
    public function deleteItem($key) {
        if (isset($this->items[$key])) {
            unset($this->items[$key]);
        }
        else {
            throw new KeyInvalidException("Invalid key $key.");
        }
    }

    /**
     * 
     * @param type $key 
     * @return type 
     * @throws KeyInvalidException
     */
    public function getItem($key) {
        if (isset($this->items[$key])) {
            return $this->items[$key];
        }
        else {
            throw new KeyInvalidException("Invalid key $key.");
        }
    }

    public function keys() {
        return array_keys($this->items);
    }

    public function length() {
        return count($this->items);
    }

    public function keyExists($key) {
        return isset($this->items[$key]);
    }
    
}
