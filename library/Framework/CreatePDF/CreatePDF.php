<?php
namespace applicationform;
// include autoloader
require_once 'library/dompdf/autoload.inc.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

/**
 * Description of CreatePDF
 *
 * @author Sydney
 */
abstract class CreatePDF {
    
    //put your code here
    public $htmlString;
    public $saveLocation;
    public $fieldValues; // is of type Collection
     
    /**
     * Populates the values into the template.
     */
    public function Populate(){      
       foreach($this->fieldValues->keys() as $keys ){
          $this->htmlString = str_replace($keys, $this->fieldValues->getItem($keys), $this->htmlString);
       }      
       foreach($this->fieldValues->keys() as $keys ){
          $this->htmlString = str_replace($keys, $this->fieldValues->getItem($keys), $this->htmlString);
       }     
    }
    
    public function GeneratePDF($cssPath, $template, $values){
        
        $this->htmlString = file_get_contents($template);
        $this->fieldValues = $values;  
        $this->Populate();
        
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        $dompdf->loadHtml($this->htmlString);
        $dompdf->set_base_path($cssPath);
        
        // (Optional) Setup the paper size and orientation
        //$dompdf->setPaper('A4', 'portrait');
        //$dompdf->set_paper(DEFAULT_PDF_PAPER_SIZE, 'portrait'); 595pt x 842pt.
        $paper_size = array(0,0,1190,1684);
        $dompdf->set_paper($paper_size);
        // Render the HTML as PDF
        $dompdf->render();
        $output = $dompdf->output();
        
        return $output;
    }
    
}
