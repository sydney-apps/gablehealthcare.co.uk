<?php

require 'PHPMailerAutoload.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PHPMailerClass
 *
 * @author Sydney
 */
class PHPMailerClass {
    //put your code here

    /**
     * 
     * @param type $from
     * @param type $recipient
     * @param type $send
     * @param type $attachment
     * @param type $message
     */
    function send($from, $recipient, $send, $attachment, $message) {

        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output
        //$mail->isSMTP();                                      // Set mailer to use SMTP
        //$mail->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
        //$mail->SMTPAuth = true;                               // Enable SMTP authentication
        //$mail->Username = 'user@example.com';                 // SMTP username
        //$mail->Password = 'secret';                           // SMTP password
        //$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        //$mail->Port = 587;                                    // TCP port to connect to

        $mail->setFrom($from, 'Online Applicant');
        $mail->addAddress($recipient, 'HR Assistant');     // Add a recipient
        //$mail->addAddress('ellen@example.com');               // Name is optional
        $mail->addReplyTo($send, 'Online Applicant');
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');
        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        $mail->addAttachment($attachment, 'CV');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'New Application';
        $mail->Body = $message;
        //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        if (!$mail->send()) {
            //echo 'Message could not be sent.';
            //var_dump('Mailer Error: ' . $mail->ErrorInfo);
            return false;
        } else {
            //echo 'Message has been sent';
            return true;
        }
    }

    /**
     * 
     * @param type $from
     * @param type $recipient
     * @param type $subject
     * @param type $message
     * @return boolean
     * 
     */
    function sendMail($from, $sendName, $recipient, $subject, $message) {

        $mail = new PHPMailer;

        $mail->setFrom($from, $sendName);
        $mail->addAddress($recipient, 'Administrator');     // Add a recipient
        //$mail->addAddress('ellen@example.com');               // Name is optional
        $mail->addReplyTo($send, $sendName);
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');

        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = $subject;
        $mail->Body = $message;
        //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        if (!$mail->send()) {
            //echo 'Message could not be sent.';
            //var_dump('Mailer Error: ' . $mail->ErrorInfo);
            return false;
        } else {
            //echo 'Message has been sent';
            return true;
        }
    }

}
