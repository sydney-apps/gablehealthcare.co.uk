<?php
include_once 'library/Framework/Collection/Collection.php';
include_once 'library/Framework/StringUtilities/StringUtilities.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IXMLReader
 *
 * @author Sydney
 */
abstract class IXMLReader {
    
    protected $Dom;
    protected $Root;
    protected $OrderedFieldsList;

    public function __construct($file_name)
    {
        $this->Dom = new DOMDocument();
        $File_Name = str_replace('\\', '\/', $file_name);
        $this->Dom->load(realpath($File_Name));
        $this->Root= $this->Dom->documentElement; 
    }  
    
    public function SaveXML(){
        return $this->Dom->saveXML();
    }
}

class ContactFormReader extends IXMLReader
{
     private $FORM_FIELD_TYPES = Array('input', 'textarea');
   
    public function GetListOfElementsByType()
    {
        $Elements_Groups = new Collection();
        foreach($this->FORM_FIELD_TYPES as $formelementtype)
        {
            $Elements_Group[0] = $this->Root->getElementsByTagName($formelementtype);
            $Elements_Groups->addItem($Elements_Group, $formelementtype);
        }
        return $Elements_Groups;
    }
    
    public function GetOrderedFieldNames(){
        $Ordered_Field_Name = new Collection();
        foreach($this->Root->getElementsByTagName('*') as $child ){
            if($child->hasAttribute('name')){
            $Ordered_Field_Name->addItem('', $child->getAttribute('name'));
            }
        }
        return $Ordered_Field_Name;
    }
}

Class FormElementTypeProcessor
{
    private $IsValidField = true;
    private $OrderedAssociatedFieldNameAndValue;
    public $RequestValues;
    
    public function __construct(Collection $OrderedElementName) {
        $this->OrderedAssociatedFieldNameAndValue = $OrderedElementName;
        $this->RequestValues = new Collection();
    }
    
    public function GetValuesCollection(){
        return $this->RequestValues;
    }
    public function ComposeMessage(){
        $message = '';
        foreach($this->OrderedAssociatedFieldNameAndValue->items as $key => $value){
            $message .= "\r\n" . ucfirst($key) . " : " . $value;
        }
        return  nl2br($message);
    }
    
    public function ValidateFields(Collection $Grouped_Elements_Node){
        foreach($Grouped_Elements_Node->items as $field_type => $field_node_list){
            $this->ValidateFieldType($field_node_list, $field_type);        
        }
    }
    
    public function ValidateFieldType($elements_of_type_list, $input_type){
        switch ($input_type){
            case 'input':
                foreach ($elements_of_type_list as $field_item){
                    $this->ValidateInputFields($field_item);
                }
                break;
            case 'textarea':
                foreach ($elements_of_type_list as $field_item){
                    $this->ValidateTextAreaFields($field_item); 
                }
                break;
            default:
                break;
        }
    }
    public function ValidateTextAreaFields($elements_of_type_list){
        foreach ($elements_of_type_list as $form_element) {
            $form_element->nodeValue = $this->ValidateTextAreaAndInputField($form_element);
        }
    }
    
    public function ValidateInputFields($elements_of_type_list){
        foreach ($elements_of_type_list as $inputbox) {        
            $inputbox->setAttribute('value', $this->ValidateTextAreaAndInputField($inputbox));
        }
    }    

    private function ValidateTextAreaAndInputField($form_element)
    {    
            $isrequired = false;
            $default = '';
            $name = '';
            if($form_element->hasAttribute('name')) $name = $form_element->getAttribute('name');
            if($form_element->hasAttribute('placeholder')) $default = $form_element->getAttribute('placeholder');
            if($form_element->hasAttribute('data-required')) $isrequired = true;
            if($form_element->hasAttribute('data-required')) $isrequired = true;
            
            $inputvalue = filter_var($_REQUEST[$name], FILTER_SANITIZE_SPECIAL_CHARS);            
            
            if(!$this->ValidateField($name, $default, $isrequired, $inputvalue))
                {    
                    $this->IsValidField = false;    
                    $form_element->setAttribute('class','form-control error');
                }  else {
                    $this->SetMessageValueForKey($inputvalue, $name);
                }
                $this->RequestValues->addItem($inputvalue, $name);
            return (StringUtilities::IsNotSetOrEmpty($inputvalue)) ? $inputvalue:"";
    }
    
    private function SetMessageValueForKey($value, $key)
    {
                $this->OrderedAssociatedFieldNameAndValue->addItem($value, $key);
    }

    /**
     * 
     * @param string $param name of input
     * @param string $default placeholder value
     * @param string $required is it optional or required
     * @return boolean
     */
    public function ValidateField($name, $default, $required, $posted_value) 
    {      
        if((!isset($name) || empty($name) || $posted_value == $default || empty($posted_value)) && $required === true)
        {
            return false;
        }else 
        {
            return (ValidateInput::OfType($default, $posted_value));
        }      
    }  
    
    public function IsMessageValid(){
        return $this->IsValidField;
    }
}