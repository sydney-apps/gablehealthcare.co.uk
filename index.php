<?php

include_once 'library/constants.php';
include('utilities/URLResolver.php');

$URLResolver = new URLResolver($_SERVER['REQUEST_URI']);
$found = $URLResolver->pageFound();

if ($found) {
    if ($URLResolver->isHomePage()) {
        include (HOME_PAGE);
    }  else {
        if(null !== ($URLResolver->getParent())){
            include $URLResolver->getParentFile();
        }else{
            include ($URLResolver->getFile());
        }
    }
} else {
    header('HTTP/1.1 200 OK');
    include (ERROR_PAGE);
    //Show 404
}

?>
