<?php
?>
            <aside class="widget widget_download_pricelist"><h5 class="widget_title">Downloads</h5>
                <ul>
                    <li class="cat-item"><a href="<?php echo APPLICATION_FORM_PDF; ?>" download><span class="sc_icon icon-file-pdf"></span>Application Form</a></li>
                    <li class="cat-item"><a href="<?php echo SERVICES_BROCHURE_PDF; ?>" download><span class="sc_icon icon-file-pdf"></span>Services Brochure</a></li>
                </ul>
            </aside>
<aside class="widget widget_contacts"><h5 class="widget_title">Contacts</h5>
    <ul>
        <!--<li class="cat-item address"></li>-->
        <li class="cat-item phone">0330 555 0033</li>
        Please call the number for your local branch.
        <br/>
        <li style="margin-top: 10px;">Doncaster</li>
        <li>Gloucestershire</li>
        <li>Norwich</li>
    </ul>
</aside>