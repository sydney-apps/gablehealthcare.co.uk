
                    <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#servicesSlider" data-slide-to="0" class="active"></li>
                            <li data-target="#servicesSlider" data-slide-to="1"></li>
                            <li data-target="#servicesSlider" data-slide-to="2"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="images/bigstock-Closeup-portrait-of-attractive-22017635.jpg" alt="Slide 1">
                            </div>

                            <div class="item">
                                <img src="images/Depositphotos_11349063_l.jpg" alt="Slide 2">
                            </div>

                            <div class="item">
                                <img src="images/Depositphotos_11349063_l.jpg" alt="Slide 3">
                            </div>

                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#servicesSlider" role="button" data-slide="prev">
                            <span class="icon-angle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#servicesSlider" role="button" data-slide="next">
                            <span class="icon-angle-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <h5 class="margin-top-large margin-bottom-small">Sed ut perspiciatis</h5>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                        laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation
                        ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor
                        in hendrerit in vulputate velit esse molestie consequat.</p>
                    <div class="sc_accordion sc_accordion_style_1 margin-top-large margin-bottom-large" id="accordion-1" role="tablist" aria-multiselectable="true">
                        <div class="panel sc_accordion_item">
                            <div class="" role="tab" id="headingOne-1">
                                <h5 class="sc_accordion_title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseOne-1" aria-expanded="true" aria-controls="collapseOne-1">
                                        Title 1
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne-1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne-1">
                                <div class="sc_accordion_content">
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                    fugiat nulla pariatur.
                                </div>
                            </div>
                        </div>
                        <div class="panel sc_accordion_item">
                            <div class="" role="tab" id="headingTwo-1">
                                <h5 class="sc_accordion_title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTwo-1" aria-expanded="false" aria-controls="collapseTwo-1">
                                        Title 2
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo-1">
                                <div class="sc_accordion_content">
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                    fugiat nulla pariatur.
                                </div>
                            </div>
                        </div>
                        <div class="panel sc_accordion_item">
                            <div class="" role="tab" id="headingThree-1">
                                <h5 class="sc_accordion_title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseThree-1" aria-expanded="false" aria-controls="collapseThree-1">
                                        Title 3
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree-1">
                                <div class="sc_accordion_content">
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                    fugiat nulla pariatur.
                                </div>
                            </div>
                        </div>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                        laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation
                        ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor
                        in hendrerit in vulputate velit.</p>
                    <p>Esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto
                        odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
