<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
    include 'includes/header.php';
    include_once 'utilities/library.php';
$ContactForm = new ContactForm();
?>
<section class="fullwidth">
        <div class="container">
            <div class="flexslider">
                <ul class="slides">
                    <!--<li>
                    <picture>
                        <source media="(min-width: 800px" srcset="images/care-assistants.png, images/care-assistants.png 2x">
                        <source media="(min-width: 450px)" srcset="images/sm-care-assistants.png, images/sm-care-assistants.png 2x">
                        <img src="images/sm-care-assistants.png" srcset="images/sm-care-assistants.png 2x" alt="Care Assistants">
                        </picture>
                        <div class="slide_description_wrapper">
                            <div class="container">
                                <div class="slide_description">
                                    <div>
                                        <h1 class="slide_title">Care and support staff</h1>

                                        <a class="btn btn-default btn-lg slide_button" href="/contact-us">Make an Appointment</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    -->
                    <li>
                    <picture>
                        <source media="(min-width: 800px" srcset="images/care-assistants.png, images/care-assistants.png 2x">
                        <source media="(min-width: 450px)" srcset="images/sm-care-assistants.png, images/sm-care-assistants.png 2x">
                        <img src="images/sm-care-assistants.png" srcset="images/sm-care-assistants.png 2x" alt="Care Assistants">
                        </picture>
                        <div class="slide_description_wrapper">
                            <div class="container">
                                <div class="slide_description">
                                    <div>
                                        <div id="cqc-report">
                                            <!--<script type="text/javascript" src="//www.cqc.org.uk/sites/all/modules/custom/cqc_widget/widget.js?data-id=1-1566922261&data-host=www.cqc.org.uk&type=location"></script>-->
                                            
                                            <!-- 23 Sep 2019 Ratings -->
                                            <script type="text/javascript" src="//www.cqc.org.uk/sites/all/modules/custom/cqc_widget/widget.js?data-id=1-7298276380&data-host=www.cqc.org.uk&type=location"></script>
                                        </div>
                                        <a class="btn btn-default btn-lg slide_button" href="/contact-us">Make an Appointment</a>
                                    </div>
                                </div>
                            </div>
                        </div>                                       
                    </li>
                    <!--
                    <li><picture>
                        <source media="(min-width: 800px" srcset="images/slide-1.jpg, images/slide-1.jpg 2x">
                        <source media="(min-width: 450px)" srcset="images/sm-slide-1.jpg, images/sm-slide-1.jpg 2x">
                        <img src="images/sm-slide-1.jpg" srcset="images/sm-slide-1.jpg 2x" alt="Care Assistants">
                        </picture>
                        <div class="slide_description_wrapper">
                            <div class="container">
                                <div class="slide_description">
                                    <div>
                                        <h1 class="slide_title">Nursing staff</h1>

                                        <a class="btn btn-default btn-lg slide_button" href="/contact-us">Make an Appointment</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <picture>
                            <source media="(min-width: 800px" srcset="images/elderly.png, images/elderly.png 2x">
                            <source media="(min-width: 450px)" srcset="images/sm-elderly.png, images/sm-elderly.png 2x">
                            <img src="images/sm-elderly.png" srcset="images/sm-elderly.png 2x" alt="Care Assistants">
                        </picture>
                        
                        <div class="slide_description_wrapper">
                            <div class="container">
                                <div class="slide_description">
                                    <div>
                                        <h1 class="slide_title">Residential care<br></h1>

                                        <a class="btn btn-default btn-lg slide_button" href="/contact-us">Make an Appointment</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                    <picture>
                            <source media="(min-width: 800px" srcset="images/slide-3.jpg, images/slide-3.jpg 2x">
                            <source media="(min-width: 450px)" srcset="images/sm-slide-3.jpg, images/sm-slide-3.jpg 2x">
                            <img src="images/sm-slide-3.jpg" srcset="images/sm-slide-3.jpg 2x" alt="Mental healthcare">
                        </picture>
                        
                        <div class="slide_description_wrapper">
                            <div class="container">
                                <div class="slide_description">
                                    <div>
                                        <h1 class="slide_title">Mental health care <br></h1>
                                        <a class="btn btn-default btn-lg slide_button" href="/contact-us">Make an Appointment</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    
                     <li>
                        <picture>
                            <source media="(min-width: 800px" srcset="images/learning-disability.png, images/learning-disability.png 2x">
                            <source media="(min-width: 450px)" srcset="images/sm-learning-disability.png, images/sm-learning-disability.png 2x">
                            <img src="images/sm-learning-disability.png" srcset="images/sm-learning-disability.png 2x" alt="Support Care">
                        </picture>
                        <div class="slide_description_wrapper">
                            <div class="container">
                                <div class="slide_description">
                                    <div>
                                        <h1 class="slide_title">Support care<br></h1>
                                        <a class="btn btn-default btn-lg slide_button" href="/contact-us">Make an Appointment</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    -->
                    
                </ul>
            </div>
        </div>
    </section>

    <section class="">
        <div class="container" style="padding-top: 34px;">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3 class="sc_title margin-bottom-large">Our Services</h3>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4 col-sm-6">
                    <div class="sc_services sc_services_style_1">
                        <a href="#" class="sc_icon sc_icon_round icon-tooth10 sc_icon_border"></a>
                        <h5 class="sc_title ">Complex care</h5>
                        <div class="sc_description">This is a service for high need service users who require one-to-one 24-hour support.</div>
                        <a href="/services/intensive-specialist-services" class="btn btn-default btn-sm center-block">More</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="sc_services sc_services_style_1">
                        <a href="#" class="sc_icon sc_icon_round icon-tooth8 sc_icon_border"></a>
                        <h5 class="sc_title ">Supported Living
                            </h5>
                        <div class="sc_description">We provide support to clients with learning difficulties </div>
                        <a href="/services/support-services" class="btn btn-default btn-sm center-block">More</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="sc_services sc_services_style_1">
                        <a href="#" class="sc_icon sc_icon_round icon-tooth11 sc_icon_border"></a>
                        <h5 class="sc_title ">Domiciliary care</h5>
                        <div class="sc_description">Providing care in the comfort of your own home.</div>
                        <a href="/services/domiciliary-care" class="btn btn-default btn-sm center-block">More</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="sc_services sc_services_style_1">
                        <a href="#" class="sc_icon sc_icon_round icon-tooth20 sc_icon_border"></a>
                        <h5 class="sc_title ">Nursing Agency </h5>
                        <div class="sc_description">We provide staffing solutions to Nursing and residential care homes, NHS Trusts, Learning disabilities centres and private hospitals for our clients.
</div>
                        <a href="/services/nursing-agency" class="btn btn-default btn-sm center-block">More</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="sc_services sc_services_style_1">
                        <a href="#" class="sc_icon sc_icon_round icon-tooth9 sc_icon_border"></a>
                        <h5 class="sc_title ">Live in care</h5>
                        <div class="sc_description">Gable Healthcare offers full time live-in care for people who do not want to leave their home life or pets and value their independence.</div>
                        <a href="/services/live-in-care" class="btn btn-default btn-sm center-block">More</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="sc_services sc_services_style_1">
                        <a href="#" class="sc_icon sc_icon_round icon-dentist6 sc_icon_border"></a>
                        <h5 class="sc_title ">Floating support</h5>
                        <div class="sc_description">We provide two different types of floating support : Housing Support <br/>
• housing related support to enable you to find and sustain a home 
</div>
                        <a href="/services/float-support" class="btn btn-default btn-sm center-block">More</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="testimonials_section" class="sc_parallax_gradient sc_parallax" data-parallax-speed="0.3" data-parallax-x-pos="50%" data-parallax-y-pos="50%">
        <div class="sc_parallax_content">
            <div class="sc_parallax_overlay">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-sm-12">
                            <h3 class="sc_title">What our patients say</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="testimonialsCarousel" class="carousel slide" data-ride="carousel">

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active sc_testimonials">
                                        <div class="sc_testimonials_item_content">
                                            <div class="sc_testimonials_item_quote">
                                                <div class="sc_testimonials_item_text">
                                                    For 1.5 hour’s daily a carer attends to me to ensure my medication and doors and windows are secure, and all electrical appliances were safely off. The carers ensure that I eat my meals and take my prescribed medication. 
                                                    
                                                    I look forward to the daily visits in the evening to have a chat and see a friendly face.

                                                </div>
                                            </div>
                                            <div class="sc_testimonials_item_author">
                                                <div class="sc_testimonials_item_name">Mr J</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="item sc_testimonials">
                                        <div class="sc_testimonials_item_content">
                                            <div class="sc_testimonials_item_quote">
                                                <div class="sc_testimonials_item_text">
                                                   After suffering a severe head injury as a result of a road traffic accident I had a number of mobility problems and needed assistance for a lot of daily activities.
Gable Health care carers were always with infinite patience and gentleness in providing aid. They helped me think clearly through the fog of emotion and provided recommendation and every gadget and facility require.

                                                </div>
                                            </div>
                                            <div class="sc_testimonials_item_author">
                                                <div class="sc_testimonials_item_name">Alan (Not real name)</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#testimonialsCarousel" role="button" data-slide="prev">
                                    <span class="icon-angle-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#testimonialsCarousel" role="button" data-slide="next">
                                    <span class="icon-angle-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="team_section" class="">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3 class="sc_title">Meet the team</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="sc_team_item sc_team_item_style_1">
                        <div class="sc_team_item_avatar"><img class="sc_image sc_image_shape_round" width="80"
                                                              height="80" alt="Registered nurses"
                                                              src="/images/our-team/nurses.png">
                        </div>
                        <div class="team_item_info-container">
                            <div class="sc_team_item_info"><h5 class="sc_team_item_title"><a
                                    href="/">Registered nurses</a></h5>

                                <div class="sc_team_item_position">Registered Nurses</div>
                                <div class="sc_team_item_description"><span class="sc_team_item_description_span">Qualified registered nurses.</span><span
                                        class="sc_team_item_description_span_icon sc_icon icon-plus sc_icon_round"></span>
                                </div>
                                <!--
                                <div class="sc_socials">
                                    <div class="sc_socials_item"><a href="#" target="_blank"
                                                                    class="sc_icon sc_icon_round icon-facebook"></a></div>
                                    <div class="sc_socials_item"><a href="#" target="_blank"
                                                                    class="sc_icon sc_icon_round icon-skype"></a></div>
                                    <div class="sc_socials_item"><a href="#" target="_blank"
                                                                    class="sc_icon sc_icon_round icon-twitter"></a></div>
                                    <div class="sc_socials_item"><a href="#" target="_blank"
                                                                    class="sc_icon sc_icon_round icon-gplus"></a></div>
                                </div>
                                -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="sc_team_item sc_team_item_style_1">
                        <div class="sc_team_item_avatar"><img class="sc_image sc_image_shape_round" width="80"
                                                              height="80" alt=""
                                                              src="/images/our-team/care-assistants.png">
                        </div>
                        <div class="team_item_info-container">
                            <div class="sc_team_item_info"><h5 class="sc_team_item_title"><a
                                    href="/">Care Assistants</a></h5>

                                <div class="sc_team_item_position">Care Assistants</div>
                                <div class="sc_team_item_description"><span class="sc_team_item_description_span">Fully trained care assistants</span><span
                                        class="sc_team_item_description_span_icon sc_icon icon-plus sc_icon_round"></span>
                                </div>
                                <!-- 
                                <div class="sc_socials">
                                    <div class="sc_socials_item"><a href="#" target="_blank"
                                                                    class="sc_icon sc_icon_round icon-facebook"></a></div>
                                    <div class="sc_socials_item"><a href="#" target="_blank"
                                                                    class="sc_icon sc_icon_round icon-skype"></a></div>
                                    <div class="sc_socials_item"><a href="#" target="_blank"
                                                                    class="sc_icon sc_icon_round icon-twitter"></a></div>
                                    <div class="sc_socials_item"><a href="#" target="_blank"
                                                                    class="sc_icon sc_icon_round icon-gplus"></a></div>
                                </div>
                                -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="sc_team_item sc_team_item_style_1">
                        <div class="sc_team_item_avatar"><img class="sc_image sc_image_shape_round" width="80"
                                                              height="80" alt="Support workers"
                                                              src="/images/our-team/support-workers.png">
                        </div>
                        <div class="team_item_info-container">
                            <div class="sc_team_item_info"><h5 class="sc_team_item_title"><a
                                    href="/">Support Worker</a></h5>

                                <div class="sc_team_item_position">Support Workers</div>
                                <div class="sc_team_item_description"><span class="sc_team_item_description_span">Experienced support workers.</span><span
                                        class="sc_team_item_description_span_icon sc_icon icon-plus sc_icon_round"></span>
                                </div>
                                <!--
                                <div class="sc_socials">
                                    <div class="sc_socials_item"><a href="#" target="_blank"
                                                                    class="sc_icon sc_icon_round icon-facebook"></a></div>
                                    <div class="sc_socials_item"><a href="#" target="_blank"
                                                                    class="sc_icon sc_icon_round icon-skype"></a></div>
                                    <div class="sc_socials_item"><a href="#" target="_blank"
                                                                    class="sc_icon sc_icon_round icon-twitter"></a></div>
                                    <div class="sc_socials_item"><a href="#" target="_blank"
                                                                    class="sc_icon sc_icon_round icon-gplus"></a></div>
                                </div>
                                 -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="gallery_section" class="light_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3 class="sc_title">Our Clients and partnerships</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="post_item post_item_square">
                        <div class="post_content ih-item circle effect5 top_to_bottom">
                            <div class="post_featured img">
                                <img class="wp-post-image" width="250" height="250" alt="NHS" src="/images/clients-partners/nhs.png">						</div>
                            <div class="post_info_wrap info">
                                <div class="info-back">
                                    <h4 class="post_title">NHS</h4>
                                    <div class="post_descr">
                                        <p></p>
                                        <span class="hover_icon icon-plus-2"></span>
                                    </div>
                                </div>
                            </div>	<!-- /.info-back /.info -->
                        </div>				<!-- /.post_content -->
                    </div>	<!-- /.post_item -->
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="post_item post_item_square">
                        <div class="post_content ih-item circle effect5 top_to_bottom">
                            <div class="post_featured img">
                                <img class="wp-post-image" width="250" height="250" alt="Royal Kingstone" src="/images/clients-partners/RoyalKingStone.png">						</div>
                            <div class="post_info_wrap info">
                                <div class="info-back">
                                    <h4 class="post_title">RoyayKingstone</h4>
                                    <div class="post_descr">
                                        <p></p>
                                        <span class="hover_icon icon-plus-2"></span>
                                    </div>
                                </div>
                            </div>	<!-- /.info-back /.info -->
                        </div>				<!-- /.post_content -->
                    </div>	<!-- /.post_item -->
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="post_item post_item_square">
                        <div class="post_content ih-item circle effect5 top_to_bottom">
                            <div class="post_featured img">
                                <img class="wp-post-image" width="250" height="250" alt="Investors in people" src="/images/clients-partners/investors-in-people.png">						</div>
                            <div class="post_info_wrap info">
                                <div class="info-back">
                                    <h4 class="post_title">Investors in people</h4>
                                    <div class="post_descr">
                                        <p></p>
                                        <span class="hover_icon icon-plus-2"></span>
                                    </div>
                                </div>
                            </div>	<!-- /.info-back /.info -->
                        </div>				<!-- /.post_content -->
                    </div>	<!-- /.post_item -->
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="post_item post_item_square">
                        <div class="post_content ih-item circle effect5 top_to_bottom">
                            <div class="post_featured img">
                                <img class="wp-post-image" width="250" height="250" alt="Federation of small businesses" src="/images/clients-partners/federation-of-small-business.png">						</div>
                            <div class="post_info_wrap info">
                                <div class="info-back">
                                    <h4 class="post_title">Federation of small businesses</h4>
                                    <div class="post_descr">
                                        <p></p>
                                        <span class="hover_icon icon-plus-2"></span>
                                    </div>
                                </div>
                            </div>	<!-- /.info-back /.info -->
                        </div>				<!-- /.post_content -->
                    </div>	<!-- /.post_item -->
                </div>
            </div>
        </div>
    </section>

    <section id="contact_form_section" class="sc_parallax_gradient sc_parallax" data-parallax-speed="0.3" data-parallax-x-pos="50%" data-parallax-y-pos="50%">
        <div class="sc_parallax_content">
            <div class="sc_parallax_overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h3 class="sc_title"> <?php $ContactForm->ShowFormTitle(); ?></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1">
                            <form role="form" action="/" method="POST" id="contactForm" data-toggle="validator" class="shake">
                                <?php 
                                    $ContactForm->DisplayForm();
                                ?>
                                <input type="hidden" data-form-title="" name="issend" value="issend" id="issend"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="map_section" class="">
        <div class="row text-center">
            <div class="col-sm-12">
                <div id="map" class="itited"></div>
            </div>
        </div>
    </section>
    <?php 
    include 'includes/footer.php';
    ?>
