<?php

    include ('includes/header.php');
    include_once 'utilities/library.php'; 
    $ContactForm = new ContactForm();

    ?>
<section>
                        <section id="map_section" class="">
                                <div class="row text-center">
                                    <div class="col-sm-12">
                                        <div id="map" class="itited" style="height: 350px;"></div>
                                    </div>
                                </div>
                            </section>
    
    <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
        
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li id="lione" data-target="#servicesSlider" data-slide-to="0"></li>
                    <li id="litwo" data-target="#servicesSlider" data-slide-to="1"></li>
                    <!--
                    <li id="lithree" data-target="#servicesSlider" data-slide-to="2"></li>
                    <li id="lifour" data-target="#servicesSlider" data-slide-to="3"></li>                    
                    <li id="lifive" data-target="#servicesSlider" data-slide-to="4"></li>                    
                    <li id="lisix" data-target="#servicesSlider" data-slide-to="5"></li>                    
                    <li id="liseven" data-target="#servicesSlider" data-slide-to="6"></li>
                    -->
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">

                    <!--<div class="item active">  
                            <section id="contact_info_section_2">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-4 text-left"><span class="address">141 Morden road,<br/> Mitcham, Surrey,<br/> CR4 4DG</span></div>
                                        <div class="col-sm-4 text-center"><span class="phone">0330 555 0033</span></div>
                                        <div class="col-sm-4 text-right timetable"><span class=""><strong>Mon-Sat: </strong>9:00am - 5:00pm 
                                        <!-- <strong>Sn: </strong>8:00am - 5:00pm</span>-->
                                        <!--
                                        </div>
                                    </div>
                                </div>
                            </section>
                    </div>
                    
                    
                    <div class="item"> 
                            <section id="contact_info_section_2">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-4 text-left"><span class="address">Edinburgh House,<br/> 7 Corporation Street,<br/> Corby, NN17 1NG</span></div>
                                        <div class="col-sm-4 text-center"><span class="phone">0330 555 0033</span></div>
                                        <div class="col-sm-4 text-right timetable"><span class=""><strong>Mon-Sat: </strong>9:00am - 5:00pm 
                                        <!-- <strong>Sn: </strong>8:00am - 5:00pm</span>--> <!--</div>
                                    </div>
                                </div>
                            </section>
                    </div>
                    
                    <div class="item"> 
                            <section id="contact_info_section_2">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-4 text-left"><span class="address">AGL House, Office 0206,<br/> 133 Birmingham Road,<br/> West Bromwich, B71 4JJ</span></div>
                                        <div class="col-sm-4 text-center"><span class="phone">0330 555 0033</span></div>
                                        <div class="col-sm-4 text-right timetable"><span class=""><strong>Mon-Sat: </strong>9:00am - 5:00pm 
                                        <!-- <strong>Sn: </strong>8:00am - 5:00pm</span>--><!--</div>
                                    </div>
                                </div>
                            </section>
                    </div>
                    
                    <div class="item"> 
                            <section id="contact_info_section_2">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-4 text-left"><span class="address">Doncaster</span></div>
                                        <div class="col-sm-4 text-center"><span class="phone">0330 555 0033</span></div>
                                        <div class="col-sm-4 text-right timetable"><span class=""><strong>Mon-Sat: </strong>9:00am - 5:00pm 
                                        <!-- <strong>Sn: </strong>8:00am - 5:00pm</span>--><!--</div>
                                    </div>
                                </div>
                            </section>
                    </div>
                    
                     <div class="item"> 
                            <section id="contact_info_section_2">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-4 text-left"><span class="address">Glocestershire</span></div>
                                        <div class="col-sm-4 text-center"><span class="phone">0330 555 0033</span></div>
                                        <div class="col-sm-4 text-right timetable"><span class=""><strong>Mon-Sat: </strong>9:00am - 5:00pm 
                                        <!-- <strong>Sn: </strong>8:00am - 5:00pm</span>--><!--</div>
                                    </div>
                                </div>
                            </section>
                    </div>

                    <div class="item"> 
                            <section id="contact_info_section_2">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-4 text-left"><span class="address">Norwich</span></div>
                                        <div class="col-sm-4 text-center"><span class="phone">0330 555 0033</span></div>
                                        <div class="col-sm-4 text-right timetable"><span class=""><strong>Mon-Sat: </strong>9:00am - 5:00pm 
                                        <!-- <strong>Sn: </strong>8:00am - 5:00pm</span>--><!--
                                        </div>
                                    </div>
                                </div>
                            </section>
                    </div>
                    -->
                    
                    <div class="item  active"> 
                            <section id="contact_info_section_2">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-4 text-left"><span class="address">Moulton Park Business Centre, <br/>Redhouse Road, <br/>Moulton Park Industrial Estate, Northampton NN3 6AQ</span>
                                        </div>
                                        <div class="col-sm-4 text-center"><span class="phone">0330 555 0033</span></div>
                                        <div class="col-sm-4 text-right timetable"><span class=""><strong>Mon-Sat: </strong>9:00am - 5:00pm <!-- <strong>Sn: </strong>8:00am - 5:00pm</span>--></div>
                                    </div>
                                </div>
                            </section>
                    </div>                   
                    <div class="item"> 
                            <section id="contact_info_section_2">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-4 text-left"><span class="address">ALFA House, Molesey Road, <br/>Walton-On-Thames, <br/>Surrey, KT12 3PD</span></div>
                                        <div class="col-sm-4 text-center"><span class="phone">0330 555 0033</span></div>
                                        <div class="col-sm-4 text-right timetable"><span class=""><strong>Mon-Sat: </strong>9:00am - 5:00pm <!-- <strong>Sn: </strong>8:00am - 5:00pm</span>--></div>
                                    </div>
                                </div>
                            </section>
                    </div>
                </div>
    </div>
</section>
        <section id="contact_form_section_2" class="light_section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h3 class="sc_title" id="form-title">
                           <?php $ContactForm->ShowFormTitle(); ?>
                        </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <form role="form" method="POST" action="/contact-us" id="contactForm" data-toggle="validator" class="shake">
                           <?php 
                                $ContactForm->DisplayForm(); 
                           ?>
                            <input type="hidden" data-form-title="" name="issend" value="issend" id="issend"/>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    <?php 

    include_once 'includes/footer.php';
  ?>