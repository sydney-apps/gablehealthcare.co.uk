        <div class="col-md-9 col-sm-8 content">
            <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#servicesSlider" data-slide-to="0" class="active"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="/images/pages/partners.png" alt="Slide 1">
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#servicesSlider" role="button" data-slide="prev">
                    <span class="icon-angle-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#servicesSlider" role="button" data-slide="next">
                    <span class="icon-angle-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <h5 class="margin-top-large margin-bottom-small">Our Partners</h5>
            <p>Gable Healthcare has a lot of experience of working in partnership with stakeholders to achieve valued outcomes for individuals and services.</p>
            <p>We are experienced in communicating, engaging and working positively with individuals and organisations such as service users, carers, families, advocates, all members of the multi-disciplinary team, housing providers, employment services, i.e., Job Centre Plus, Learn Direct, local educational and vocational providers, other independent providers of services, health services, clinical teams, commissioning and procurement services, voluntary sector groups, community groups and societies and members of the local community.
                We work hard to establish and maintain positive working relationships with all stakeholders to provide an all round quality service. We achieve this by having strong, visible management structures and working in a warm, friendly, accountable and professional way.</p>
            <p>At Gable Healthcare Services we deliver our services with a problem solving and “can do” approach. We recognise that issues such as limited time and resources can get in the way of effective partnership working and of reaching desired service outcomes and we work to overcome this and assure the quality of our services by:</p>
            <ul>
            <li>Having shared values and a vision for the service </li>
            <li>Pooling and maximising resources 
            <li>Having in place clear desired outcomes and expectations for the service, with the commitment to meet and exceed them </li>
            <li>Working to the service specification and contract </li>
            <li>Having accountable and transparent line management systems </li>
            <li>Positively engaging with internal and external quality monitoring procedures (for example, CQC and Gable Healthcare Services’s internal quality audit system)</li>
            <li>Having a culture of continuous improvement </li>
            <li>Having open lines of communication and engagement with partner agencies and all stakeholders </li>
            <li>Working collaboratively in overcoming issues and meeting the needs of individuals </li>
            </ul>
            <div class="sc_accordion sc_accordion_style_1 margin-top-large margin-bottom-large" id="accordion-1" role="tablist" aria-multiselectable="true">
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingOne-1">
                        <h5 class="sc_accordion_title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseOne-1" aria-expanded="true" aria-controls="collapseOne-1">
                                Working with Families
                            </a>
                        </h5>
                    </div>
                    <div id="collapseOne-1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne-1">
                        <div class="sc_accordion_content">
                            When working with families we make sure that they are included in the development and planning of services and where appropriate are actively included in an individual’s support planning. Within a service, we make sure that family members understand the structure of the service and can communicate openly with staff and express their views and feelings about the service; have named contacts for senior staff; have background information on the organisation and understand how to give feedback and have a voice. Staff are trained to work positively with families and carers and see the benefits of working together.                        </div>
                    </div>
                </div>
                
            </div>
</div>