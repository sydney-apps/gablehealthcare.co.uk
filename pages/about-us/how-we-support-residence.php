        <div class="col-md-9 col-sm-8 content">
            <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#servicesSlider" data-slide-to="0" class="active"></li>
                   <!-- <li data-target="#servicesSlider" data-slide-to="1"></li>
                    <li data-target="#servicesSlider" data-slide-to="2"></li>
                   -->
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="/images/pages/supported-living.png" alt="Slide 1">
                    </div>

   <!--                 <div class="item">
                        <img src="images/Depositphotos_11349063_l.jpg" alt="Slide 2">
                    </div>

                    <div class="item">
                        <img src="images/Depositphotos_11349063_l.jpg" alt="Slide 3">
                    </div>
-->
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#servicesSlider" role="button" data-slide="prev">
                    <span class="icon-angle-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#servicesSlider" role="button" data-slide="next">
                    <span class="icon-angle-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <h5 class="margin-top-large margin-bottom-small">How we support residents</h5>
            <div class="sc_accordion sc_accordion_style_1 margin-top-large margin-bottom-large" id="accordion-1" role="tablist" aria-multiselectable="true">
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingOne-1">
                        <h5 class="sc_accordion_title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseOne-1" aria-expanded="true" aria-controls="collapseOne-1">
                                Financial Matters
                            </a>
                        </h5>
                    </div>
                    <div id="collapseOne-1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne-1">
                        <div class="sc_accordion_content">
Help with managing money, advice on budgeting and practical aspects of paying bills (including the rent).
Help to sort out debts and affordable repayment plans. 
This may involve referring you to a specialist advice agency 
                        </div>
                    </div>
                </div>
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingTwo-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTwo-1" aria-expanded="false" aria-controls="collapseTwo-1">
                                Welfare Benefits 
                            </a>
                        </h5>
                    </div>
                    <div id="collapseTwo-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo-1">
                        <div class="sc_accordion_content">
                            Advice and help and with making claims, filling out forms and following up entitlements with the Benefits Agency or Housing Benefit.
                        </div>
                    </div>
                </div>
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingThree-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseThree-1" aria-expanded="false" aria-controls="collapseThree-1">
                               Managing your home/Sustaning your tenancy
                            </a>
                        </h5>
                    </div>
                    <div id="collapseThree-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree-1">
                        <div class="sc_accordion_content">
                            Help to organise the running of your home
Help in sorting out problems with neighbours
Help to develop your home making skills e.g. cooking, housekeeping, health and safety, DIY
                        </div>
                    </div>
                </div>
                
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingFour-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFour-1" aria-expanded="false" aria-controls="collapseFour-1">
                               Training
                            </a>
                        </h5>
                    </div>
                    <div id="collapseFour-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour-1">
                        <div class="sc_accordion_content">
                            Help to identify and follow up training and employment opportunities
                        </div>
                    </div>
                </div> 
                   <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingFive-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFive-1" aria-expanded="false" aria-controls="collapseFive-1">
                               Employment and Leisure
                            </a>
                        </h5>
                    </div>
                    <div id="collapseFive-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive-1">
                        <div class="sc_accordion_content">
                             Advice and help with taking up leisure and social activities.
                        </div>
                    </div>
                </div>
                
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingSix-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseSix-1" aria-expanded="false" aria-controls="collapseSix-1">
                               Parental Skills
                            </a>
                        </h5>
                    </div>
                    <div id="collapseSix-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix-1">
                        <div class="sc_accordion_content">
                            Support to develop positive parenting and childcare skills 
                        </div>
                    </div>
                </div>
                
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingSeven-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseSeven-1" aria-expanded="false" aria-controls="collapseSeven-1">
                            Healthy Lifestyles
                            </a>
                        </h5>
                    </div>
                    <div id="collapseSeven-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven-1">
                        <div class="sc_accordion_content">
                              WPromote healthy lifestyles such as sexual health, nutrition awareneww and leisure fitness activities. 
                        </div>
                    </div>
                </div>
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingEleven-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseEight-1" aria-expanded="false" aria-controls="collapseEight-1">
                               Education and training
                            </a>
                        </h5>
                    </div>
                    <div id="collapseEight-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight-1">
                        <div class="sc_accordion_content">
                               Support to access education and training opportunities 
                        </div>
                    </div>
                </div>
                
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingNine-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseNine-1" aria-expanded="false" aria-controls="collapseNine-1">
                               Personal
                            </a>
                        </h5>
                    </div>
                    <div id="collapseNine-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine-1">
                        <div class="sc_accordion_content">
                               Support to deal with difficult problems including loneliness, drug and alcohol use, health and emotional problems.
                        </div>
                    </div>
                </div>
                
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingTen-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTen-1" aria-expanded="false" aria-controls="collapseTen-1">
                              Support
                            </a>
                        </h5>
                    </div>
                    <div id="collapseTen-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen-1">
                        <div class="sc_accordion_content">
                            <p>Help to put you in contact with other specialist organisations e.g. counselling agencies, legal advice, medical services, social services
                               <ul><li>
                                   To ensure your cultural and religious needs are met regular house meetings, and all residents are expected to attend.
                                   </li></ul>
Meetings will be held monthly, but may be more often depending on whether residents would like them to be more frequent. 
                        </p>
                        </div>
                    </div>
                </div>
                
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingEleven-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseEleven-1" aria-expanded="false" aria-controls="collapseEleven-1">
                               Debt Management
                            </a>
                        </h5>
                    </div>
                    <div id="collapseEleven-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven-1">
                        <div class="sc_accordion_content">
                               If you require support and advise with managing your debts please speak to your house manager 
                        </div>
                    </div>
                </div>
            </div>
            <p></p>
        </div>
