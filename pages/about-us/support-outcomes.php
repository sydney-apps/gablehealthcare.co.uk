     <div class="col-md-9 col-sm-8 content">
            <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#servicesSlider" data-slide-to="0" class="active"></li>
                    <!-- <li data-target="#servicesSlider" data-slide-to="1"></li>
                    <li data-target="#servicesSlider" data-slide-to="2"></li> -->
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="/images/pages/supported-living.png" alt="Slide 1">
                    </div>
<!--
                    <div class="item">
                        <img src="/images/Depositphotos_11349063_l.jpg" alt="Slide 2">
                    </div>

                    <div class="item">
                        <img src="/images/Depositphotos_11349063_l.jpg" alt="Slide 3">
                    </div>
-->
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#servicesSlider" role="button" data-slide="prev">
                    <span class="icon-angle-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#servicesSlider" role="button" data-slide="next">
                    <span class="icon-angle-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <h5 class="margin-top-large margin-bottom-small">Outcomes for People we Support</h5>
            <p>Gable Healthcare is committed to an approach and philosophy that enables service users to achieve the following:</p>
            <div class="sc_accordion sc_accordion_style_1 margin-top-large margin-bottom-large" id="accordion-1" role="tablist" aria-multiselectable="true">
                
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingOne-1">
                        <h5 class="sc_accordion_title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseOne-1" aria-expanded="true" aria-controls="collapseOne-1">
                                Stay safe
                            </a>
                        </h5>
                    </div>
                    <div id="collapseOne-1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne-1">
                        <div class="sc_accordion_content">
                            <p>Gable Healthcare is committed to delivering its services in a manner which promotes the safety and security of service users at all times. We have a comprehensive Safeguarding Adults Policy and a Whistleblowing Policy which underpins our commitment to ensuring the wellbeing and safety of all individuals.
                            </p><p>We believe in a positive approach to risk taking whereby people can experience important opportunities for personal development whilst also being kept safe from harm in accordance with our duty of care</p>
                        </div>
                    </div>
                </div>
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingTwo-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTwo-1" aria-expanded="false" aria-controls="collapseTwo-1">
                                Be healthy
                            </a>
                        </h5>
                    </div>
                    <div id="collapseTwo-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo-1">
                        <div class="sc_accordion_content">
                            <p>We promote health and wellbeing as a key theme within support planning and person centred planning. All service users have detailed Health Action Plans to ensure that their health needs are met.
</p><p>Within our services there is an emphasis on healthy living, eating, exercise and health promotion to enable service users to improve their physical and mental wellbeing and to manage their own healthcare needs as fully as possible. 
</p><p>Service users are supported to access all of the health care resources available to them and to receive a level of proactive and preventative healthcare which is as good as or better than that enjoyed by the general population.
<p></p>Where a service user requires support with personal care this is delivered in such a way as to protect and promote their dignity whilst also maximising their independence wherever possible</p>
                        </div>
                    </div>
                </div>
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingThree-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseThree-1" aria-expanded="false" aria-controls="collapseThree-1">
                                Enjoy and achieve
                            </a>
                        </h5>
                    </div>
                    <div id="collapseFour-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree-1">
                        <div class="sc_accordion_content">
                            Gable Healthcare we are committed to delivering our services in ways that promote both enjoyment and achievement for the people that we support. We support people to have access to leisure, social activities, life long learning and to universal public and commercial services and work in partnership with other agencies and service to achieve this.
                        </div>
                    </div>
                </div>
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingFour-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFour-1" aria-expanded="false" aria-controls="collapseFour-1">
                                Make a positive contribution
                            </a>
                        </h5>
                    </div>
                    <div id="collapseFour-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour-1">
                        <div class="sc_accordion_content">
                            <p>
                                We are committed to ensuring that all individuals are able to make positive contributions both within their own lives and within their local communities.</p>
                            <p>
Our services are characterised by a high level of service user involvement. User involvement adds value our services and provides an essential resource in terms of peer support for other service users.
Service users are involved in the design, planning and delivery of our services. Activities include:</p>
                            <ul>
                                <li>Activities in community settings</li>
                                <li>Activities, sessions and groups within Day services</li>
                                <li>Developing support networks</li>
                                <li>Involvement in the promotion of the service</li>
                                <li>Staff recruitment and training</li>
                                <li>Involvement with designing and updating policies and procedures</li>
                                <li>Involvement on Partnership Boards</li>
                                <li>Leadership and Governance forums </li>
                                <li>Involvement in the promotion of mental health awareness and destigmatisation</li>
                                <li>The creation of links with wider community resources</li>
                                <li>Developing a local business plan</li>
                                <li>Organisation of events</li>
                                <li>We aim to build on service users existing skills and interests and utilise the resources to develop these</li>
</ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingFive-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFive-1" aria-expanded="false" aria-controls="collapseFive-1">
                                Achieve economic well-being
                            </a>
                        </h5>
                    </div>
                    <div id="collapseFive-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive-1">
                        <div class="sc_accordion_content">
                            <p>We believe it is essential for the people we support to have access to income and resources sufficient for a good diet, accommodation and participation in family and community life. It is also important that people can meet costs arising from individual needs that they may have.
            </p><p>We support service users to effectively budget their finances where this is an identified need and our supporting team can provide expert advice and support in this area.
</p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>