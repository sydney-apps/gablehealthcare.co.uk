        <div class="col-md-9 col-sm-8 content">
            <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#servicesSlider" data-slide-to="0" class="active"></li>
                   <!-- <li data-target="#servicesSlider" data-slide-to="1"></li>
                    <li data-target="#servicesSlider" data-slide-to="2"></li>
                   -->
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="/images/pages/intensive-care-service.png" alt="Slide 1">
                    </div>

   <!--                 <div class="item">
                        <img src="images/Depositphotos_11349063_l.jpg" alt="Slide 2">
                    </div>

                    <div class="item">
                        <img src="images/Depositphotos_11349063_l.jpg" alt="Slide 3">
                    </div>
-->
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#servicesSlider" role="button" data-slide="prev">
                    <span class="icon-angle-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#servicesSlider" role="button" data-slide="next">
                    <span class="icon-angle-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <h5 class="margin-top-large margin-bottom-small">Why Choose us</h5>
            <div class="sc_accordion sc_accordion_style_1 margin-top-large margin-bottom-large" id="accordion-1" role="tablist" aria-multiselectable="true">
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingOne-1">
                        <h5 class="sc_accordion_title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseOne-1" aria-expanded="true" aria-controls="collapseOne-1">
                                Reliability
                            </a>
                        </h5>
                    </div>
                    <div id="collapseOne-1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne-1">
                        <div class="sc_accordion_content">
We are reliable and can reach us on our 24hour contact: both during and out of hours.
                        </div>
                    </div>
                </div>
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingTwo-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTwo-1" aria-expanded="false" aria-controls="collapseTwo-1">
                                Vetting: 
                            </a>
                        </h5>
                    </div>
                    <div id="collapseTwo-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo-1">
                        <div class="sc_accordion_content">
                            all our staff undergo a rigorous recruitment process which they have to pass before attending to a client. They each have a current CRB (Criminal Records Bureau) disclosure and ISA checked and are issued with identity cards for added peace of mind.
                        </div>
                    </div>
                </div>
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingThree-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseThree-1" aria-expanded="false" aria-controls="collapseThree-1">
                               Training: 
                            </a>
                        </h5>
                    </div>
                    <div id="collapseThree-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree-1">
                        <div class="sc_accordion_content">
                            We offer continued professional development opportunities in conjunction with NVQ providers and local Education Providers.<br/> 
                            We conduct a 6 months review as well as 360 degree performance appraisal.
                        </div>
                    </div>
                </div>
                
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingFour-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFour-1" aria-expanded="false" aria-controls="collapseFour-1">
                               Quality:
                            </a>
                        </h5>
                    </div>
                    <div id="collapseFour-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour-1">
                        <div class="sc_accordion_content">
                            We always ensure that we provide the best care possible both in quality and of good value. Value for money: all our charges are in line with the CQC and local authority quality preference. 
                        </div>
                    </div>
                </div> 
                   <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingFive-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFive-1" aria-expanded="false" aria-controls="collapseFive-1">
                               Regular spot checks:
                            </a>
                        </h5>
                    </div>
                    <div id="collapseFive-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive-1">
                        <div class="sc_accordion_content">
                             We undertake regular spot checks on staff to ensure they continue to meet the standards we expect from them.

                        </div>
                    </div>
                </div>
                
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingSix-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseSix-1" aria-expanded="false" aria-controls="collapseSix-1">
                               Feedback from our customer: 
                            </a>
                        </h5>
                    </div>
                    <div id="collapseSix-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix-1">
                        <div class="sc_accordion_content">
                             We obtain regular feedback on staff performance from all our clients to ensure we maintain the very highest standards and quality you rightly expect.

                        </div>
                    </div>
                </div>
                
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingSeven-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseSeven-1" aria-expanded="false" aria-controls="collapseSeven-1">
                               

Profiling:                            </a>
                        </h5>
                    </div>
                    <div id="collapseSeven-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven-1">
                        <div class="sc_accordion_content">
                              We profile each and every client to ensure we match our staff to your requirements.
                        </div>
                    </div>
                </div>
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingEight-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseEight-1" aria-expanded="false" aria-controls="collapseEight-1">
                               
                
                Complaints:                            </a>
                        </h5>
                    </div>
                    <div id="collapseEight-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight-1">
                        <div class="sc_accordion_content">
                               If anything does go wrong, or is not to your satisfaction, we have a clear complaints procedure to sort things out.
                        </div>
                    </div>
                </div>
            </div>
            <p></p>
        </div>
