<div class="col-md-9 col-sm-8 content">
            <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#servicesSlider" data-slide-to="0" class="active"></li>
                    <!--
                    <li data-target="#servicesSlider" data-slide-to="1"></li>
                    <li data-target="#servicesSlider" data-slide-to="2"></li>
                    -->
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="/images/pages/our-staff.png" alt="Slide 1">
                    </div>
<!--
                    <div class="item">
                        <img src="images/Depositphotos_11349063_l.jpg" alt="Slide 2">
                    </div>

                    <div class="item">
                        <img src="images/Depositphotos_11349063_l.jpg" alt="Slide 3">
                    </div>
-->
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#servicesSlider" role="button" data-slide="prev">
                    <span class="icon-angle-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#servicesSlider" role="button" data-slide="next">
                    <span class="icon-angle-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <h5 class="margin-top-large margin-bottom-small">Who we are</h5>
            <p>Gable Healthcare Services is a leading provider of high quality supported housing and domiciliary care services for vulnerable adults with enduring mental illness, learning disabilities, substance/alcohol misuse, and other complex needs.</p>
            <div class="sc_accordion sc_accordion_style_1 margin-top-large margin-bottom-large" id="accordion-1" role="tablist" aria-multiselectable="true">
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingOne-1">
                        <h5 class="sc_accordion_title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseOne-1" aria-expanded="true" aria-controls="collapseOne-1">
                                Our History
                            </a>
                        </h5>
                    </div>
                    <div id="collapseOne-1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne-1">
                        <div class="sc_accordion_content">
                            <p>
                                Gable Healthcare’s management team has had extensive experience of providing both health and social care services over the last 10 years. Our Chief Executive has been involved as a practitioner in the NHS since 2006. As a mental health nurse practitioner working alongside GP (General Practitioner) he had been involved in the provision of services in the community setting out to develop a range of innovative services in partnership with a wide range of agencies.
                            </p>
                            <p>
                                Our continuing mission is to provide reliable, respectful services which enable service users to reach their individual potential and enjoy a high quality of life. We aim to achieve this whilst providing best value to purchasers and using our resources in efficient, creative and flexible ways. 
                            </p>
                            <p>
                                Everyone we support is at the centre of everything we do..
                            </p>
                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
