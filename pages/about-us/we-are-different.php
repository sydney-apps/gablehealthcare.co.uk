        <div class="col-md-9 col-sm-8 content">
            <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#servicesSlider" data-slide-to="0" class="active"></li>
                    <!-- 
                    <li data-target="#servicesSlider" data-slide-to="1"></li>
                    <li data-target="#servicesSlider" data-slide-to="2"></li>
                    -->
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="/images/pages/domiciliary-care.png" alt="Slide 1">
                    </div>
<!--
                    <div class="item">
                        <img src="images/Depositphotos_11349063_l.jpg" alt="Slide 2">
                    </div>

                    <div class="item">
                        <img src="images/Depositphotos_11349063_l.jpg" alt="Slide 3">
                    </div>
-->
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#servicesSlider" role="button" data-slide="prev">
                    <span class="icon-angle-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#servicesSlider" role="button" data-slide="next">
                    <span class="icon-angle-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <h5 class="margin-top-large margin-bottom-small">What makes us different</h5>
            <p>The people we support are at the centre of everything we do.</p>
            <div class="sc_accordion sc_accordion_style_1 margin-top-large margin-bottom-large" id="accordion-1" role="tablist" aria-multiselectable="true">
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingOne-1">
                        <h5 class="sc_accordion_title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseOne-1" aria-expanded="true" aria-controls="collapseOne-1">
                                Why to choose us as your Live in Care provider? 
                            </a>
                        </h5>
                    </div>
                    <div id="collapseOne-1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne-1">
                        <div class="sc_accordion_content">
                                    <ul>
                                        <li>We have vast Experience in delivering Live in Care</li>
                                        <li>We can be Trusted</li>
                                        <li>Comfort</li>
                                        <li>Peace of mind – by choosing us you will have the peace of mind, even if you ar abroad. We can manage the welfare of your loved ones.</li>
                                        <li>Continuity of care – same carers all the time</li>
                                    </ul>
                        </div>
                    </div>
                </div>
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingTwo-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTwo-1" aria-expanded="false" aria-controls="collapseTwo-1">
                                Our aim
                            </a>
                        </h5>
                    </div>
                    <div id="collapseTwo-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo-1">
                        <div class="sc_accordion_content">
                            <ul>
                                <li>Support that meets client’s individual needs</li>
                                <li>We continually strive for innovation and for new ways to improve the services that we offer.</li>
                                <li>High quality services - Services where individual choice is respected, promoted and encouraged</li>
                                <li>Quality checks which monitors and review our systems to ensure the highest standards of care is delivered.</li>
                            </ul>
                            
                        </div>
                    </div>
                </div>
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingThree-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseThree-1" aria-expanded="false" aria-controls="collapseThree-1">
                               The benefits of Live in Care
                            </a>
                        </h5>
                    </div>
                    <div id="collapseThree-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree-1">
                        <div class="sc_accordion_content">
                            <ul>
                                <li>Promotes Independence</li>
                                <li>Companionship</li>
                                <li>Couples can remain living together</li>
                                <li>High level of personal one-to-one support</li>
                                <li>Minimal disruption of the lifestyle that you are used to</li>
                                <li>Individuals can stay in their own homes</li>
                                <li>You can keep your pets </li>
                                <li>Peace of mind for the individuals – that someone who is kind and caring is around</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <p>For a free cost estimation or to book a free assessment.</p>
            
        </div>