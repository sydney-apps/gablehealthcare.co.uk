<?php 

    include_once ('includes/header.php');
    include_once ('utilities/library.php'); 
    include_once ('library/Framework/CreateForm/CreateApplicationForm.php');
    
    $Form = new CreateApplicationForm();
?>
        <section id="contact_form_section_2" class="light_section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <form role="form" method="POST" action="/apply-online" id="contactForm" data-toggle="validator" class="shake">
                          <h1 class="row">
                            <a>
                              <?php $Form->ShowFormTitle(); ?>
                            </a>
                            <span class="error">
                              <?php $Form->ShowError(); ?>
                            </span>
                          </h1>
                           <?php 
                                $Form->DisplayForm(); 
                           ?>
                            <input type="hidden" data-form-title="" name="issend" value="issend" id="issend"/>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    <?php 
        include_once 'includes/footer.php';
    ?>                          
