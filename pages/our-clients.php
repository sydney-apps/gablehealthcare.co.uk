<?php
    include ('includes/header.php');
?>

<div id="services" class="container">
    <div class="row">
        <div class="col-md-3 col-sm-4 services_sidebar ">
            <h3 class="margin-top-no">Who we are</h3>
            <aside class="widget_nav_menu">
                <?php
                //include('utilities/nav/SideBarNav.php');
                //$SideBarNav = new SideBarNav('xml/about-us.xml');
                //$SideBarNav->setCurrentNode('active', 'a',$URLResolver->getCurrentPage());
                ?>
            </aside>
            <?php 
                include ('xml/sidebar/address.php');
            ?>
        </div>
        <?php
            //$filename = /pages/about-us/'.$URLResolver->getCurrentPage().'php';
            if($URLResolver->pageFound()){
                include ('pages/our-clients/'.$URLResolver->getCurrentPage().'.php'); 
            }
            ?>
        
    </div>
    
        <?php include 'includes/footer.php'; ?>
    
</div>
