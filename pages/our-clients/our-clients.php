     <div class="col-md-9 col-sm-8 content">
            <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#servicesSlider" data-slide-to="0" class="active"></li>
  <!--                  <li data-target="#servicesSlider" data-slide-to="1"></li>
                    <li data-target="#servicesSlider" data-slide-to="2"></li>
      -->          </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="/images/pages/young-people.png" alt="Slide 1">
                    </div>
<!--
                    <div class="item">
                        <img src="/images/Depositphotos_11349063_l.jpg" alt="Slide 2">
                    </div>

                    <div class="item">
                        <img src="/images/Depositphotos_11349063_l.jpg" alt="Slide 3">
                    </div>
-->
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#servicesSlider" role="button" data-slide="prev">
                    <span class="icon-angle-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#servicesSlider" role="button" data-slide="next">
                    <span class="icon-angle-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <h5 class="margin-top-large margin-bottom-small">Our clients</h5><p>
                                We are committed to involving the people who use our supported living services in all aspects of service design, delivery and evaluation.
                            </p>
                            <p>
                                We work with clients to find out how they would like to become involved and provide support, training and resources to help with this.</p>
                            <p>
                                Clients are also involved in evaluating our services to ensure they are provided to the highest quality.
                            </p>
                   
<div class="sc_accordion sc_accordion_style_1 margin-top-large margin-bottom-large" id="accordion-1" role="tablist" aria-multiselectable="true">                            
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingEight-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseEight-1" aria-expanded="true" aria-controls="collapseEight-1">
                                People with learning disabilities
                            </a>
                        </h5>
                    </div>
                    <div id="collapseEight-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight-1">
                        <div class="sc_accordion_content">
                            <p>
                                We work with people with learning disabilities to help them live the life they want. We encourage and support people to build their social networks and work towards personal goals such as learning to read and write or getting a job.We support people to maximise their choice and control over where, with whom and how they live.
                            </p>
                        </div>
                    </div>
                </div>
                            
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingFive-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFive-1" aria-expanded="true" aria-controls="collapseFive-1">
                                Recovery
                            </a>
                        </h5>
                    </div>
                    <div id="collapseFive-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive-1">
                        <div class="sc_accordion_content">
                            <p>
                                The move towards recovery is different for each person. Some people may choose to aim towards feeling content and well, while others may aim towards living the best life they can, while living with the every-day symptoms of their condition.  Our role is to support each person throughout the recovery process.
                            </p>
                        </div>
                    </div>
                </div>
                            
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingSix-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseSix-1" aria-expanded="true" aria-controls="collapseSix-1">
                                People with physical difficulties
                            </a>
                        </h5>
                    </div>
                    <div id="collapseSix-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix-1">
                        <div class="sc_accordion_content">
                            <p>
                                Our staff pride themselves on treating our clients as individuals, providing them with the best supported living available, helping them to live as independently as possible and be part of their local community.
                            </p>
                        </div>
                    </div>
                </div>
                            
                            <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingSeven-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseSeven-1" aria-expanded="true" aria-controls="collapseSeven-1">
                                Young people
                            </a>
                        </h5>
                    </div>
                    <div id="collapseSeven-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven-1">
                        <div class="sc_accordion_content">
                            <p>
                                Our staff pride themselves on treating our clients as individuals, providing them with the best supported living available, helping them to live as independently as possible and be part of their local community.
                            </p>
                        </div>
                    </div>
                </div>
    
                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingFour-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFour-1" aria-expanded="true" aria-controls="collapseFour-1">
                                Ex-offenders
                            </a>
                        </h5>
                    </div>
                    <div id="collapseFour-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour-1">
                        <div class="sc_accordion_content">
                            <p>
                                People often leave prison with underlying support needs, such as undiagnosed mental-health problems.  Coming out of prison and making a new start is particularly hard for people with mental-health problems.
                            </p>
                            <p>
                                We work closely with the probation service and other agencies to help ex-offenders to integrate back into society and access education, training and work opportunities.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="panel sc_accordion_item">
                    <div class="" role="tab" id="headingThree-1">
                        <h5 class="sc_accordion_title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseThree-1" aria-expanded="true" aria-controls="collapseThree-1">
                                Profound and Multiple Learning Disabilities (PMLD
                            </a>
                        </h5>
                    </div>
                    <div id="collapseThree-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree-1">
                        <div class="sc_accordion_content">
                            <p>
                                We support people with profound and multiple learning disabilities (PMLD) within specially adapted services. 
                            </p>
                        </div>
                    </div>
                </div>
</div>
                            <h3>Types of support</h3>
                            <p>We support people to lead happy and healthy lives. We work together with people to help them make the changes they want for a better life.</p>
                            <p>We offer:
                            <ul>
                                <li>Flexible, person-centred practical and emotional support – often in conjunction with other local services </li>
                                <li>Help so that people can build their confidence and independence </li>
                                <li>Experienced and professional staff who are well trained and supported to provide an excellent service </li>
                                <li>A commitment to ensuring our clients have increased choice and control over their lives.</li>
                                <li></li>
                                <li></li>
                                
                            </ul>
                            <p>We believe all people have the potential to find their own solutions given the right support. Our supported living staff work alongside individuals to help them take control, overcome challenges and fulfil their aspirations.</p>
                            <p>We tailor our services to suit each person. Our current services include:
                            <ul>
                                <li>Supported Living Services </li>
                                <li>Domiciliary Care and Outreach support </li>
                                <li>Support in the Community and Day Skills </li>
                                <li>Residential services and Respite Care</li>
                            </ul>
</div>