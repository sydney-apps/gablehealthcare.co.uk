<?php
    include ('includes/header.php');
?>
<div id="services" class="container">
    <div class="row">
        <div class="col-md-3 col-sm-4 services_sidebar ">
            <h3 class="margin-top-no">What we do</h3>
            <aside class="widget_nav_menu">
                <?php
                include('utilities/nav/SideBarNav.php');
                $SideBarNav = new SideBarNav('xml/services.xml');
                $SideBarNav->setCurrentNode('active', 'a',$URLResolver->getCurrentPage());
                ?>
            </aside>
<!--            <aside class="widget widget_download_pricelist"><h5 class="widget_title">Downloads</h5>
                <ul>
                    <li class="cat-item"><a href="#"><span class="sc_icon icon-file-word"></span>Application Form.doc</a></li>
                    <li class="cat-item"><a href="#"><span class="sc_icon icon-file-powerpoint"></span>Services Brochure.doc</a></li>
                    <li class="cat-item"><a href="#"><span class="sc_icon icon-file-pdf"></span>Services Picelist.pdf</a></li>
                </ul>
            </aside>
-->
            <?php 
                include ('xml/sidebar/address.php');
            ?>
        </div>
        <?php
            //$filename = /pages/about-us/'.$URLResolver->getCurrentPage().'php';
            if($URLResolver->pageFound()){
                include ('pages/services/'.$URLResolver->getCurrentPage().'.php'); 
            }
            ?>
        
    </div>
    
        <?php include 'includes/footer.php'; ?>
    
</div>
