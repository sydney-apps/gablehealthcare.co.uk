<div class="col-md-9 col-sm-8 content">
                    <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#servicesSlider" data-slide-to="0" class="active"></li>
    <!--                        <li data-target="#servicesSlider" data-slide-to="1"></li>
                            <li data-target="#servicesSlider" data-slide-to="2"></li>
         -->               </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="/images/pages/our-staff.png" alt="Slide 1">
                            </div>
<!--
                            <div class="item">
                                <img src="/images/Depositphotos_11349063_l.jpg" alt="Slide 2">
                            </div>

                            <div class="item">
                                <img src="/images/Depositphotos_11349063_l.jpg" alt="Slide 3">
                            </div>
-->
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#servicesSlider" role="button" data-slide="prev">
                            <span class="icon-angle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#servicesSlider" role="button" data-slide="next">
                            <span class="icon-angle-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
<div class="item-pagenone">
    <div class="page-header">
	<h2>
            Referals
        </h2>
	</div>
	<p>We take referrals from all sources including family and friends, GPs. Social workers, CMHT teams, private organisations and charity organisation.</p>
<p>If you are concerned about yourself or someone else please call us for an informal chat about how we can help call our Head Office:<br><br><strong>Tel</strong> &nbsp; &nbsp; : 0330 555 0033 or <br><strong>Email&nbsp;</strong>: 
 
    <a href="mailto:admin@gablehealthcare.co.uk">admin@gablehealthcare.co.uk</a><script type="text/javascript">
    <span style="display: none;">
                This email address is being protected from spambots. You need JavaScript enabled to view it.
    </span> <br><strong>Mob</strong> &nbsp; : 07809428 407 ( Out of hours line)</p>
</div>
</div>