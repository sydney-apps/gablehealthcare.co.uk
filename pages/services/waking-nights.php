<div class="col-md-9 col-sm-8 content">
                    <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#servicesSlider" data-slide-to="0" class="active"></li>
 <!--                           <li data-target="#servicesSlider" data-slide-to="1"></li>
                            <li data-target="#servicesSlider" data-slide-to="2"></li>
     -->                   </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="/images/pages/intensive-care-service.png" alt="Slide 1">
                            </div>

        <!--                    <div class="item">
                                <img src="/images/Depositphotos_11349063_l.jpg" alt="Slide 2">
                            </div>

                            <div class="item">
                                <img src="/images/Depositphotos_11349063_l.jpg" alt="Slide 3">
                            </div>
-->
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#servicesSlider" role="button" data-slide="prev">
                            <span class="icon-angle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#servicesSlider" role="button" data-slide="next">
                            <span class="icon-angle-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
    <div class="item-pagenone">
        <div class="page-header">
            <h2>
                <a href="/index.php/our-services/waking-nights"> Waking Nights</a>
            </h2>
            </div>

        <p>
            Night care may be required sometimes to provide a break for you or your family for a short duration while you recover from an accident, illness or hospital stay.
</p><p>For people who need extra support due to medical conditions, or other vulnerabilities, additional help from night time carers can mean the world of difference to you and your family. For those who suffer with progressive mental illnesses such as Alzheimer’s, broken sleep can affect your overall health, which could then lead to a necessity for further health care assistance.
</p><p>With our waking nights, one of our Care Workers will come to your home for the night and remain awake and help provide whatever assistance or monitoring is needed.
        </p> 
        <H2>
            What Does Night Home Care Provide?</h2>
        <p>
We can offer various options of flexible night care for our customers, including the following</p>
    </div>
    <div class="sc_accordion sc_accordion_style_1 margin-top-large margin-bottom-large" id="accordion-1" role="tablist" aria-multiselectable="true">
                        <div class="panel sc_accordion_item">
                            <div class="" role="tab" id="headingOne-1">
                                <h5 class="sc_accordion_title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseOne-1" aria-expanded="true" aria-controls="collapseOne-1">
                                        
Sleeping Night
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne-1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne-1">
                                <div class="sc_accordion_content">
                                    <p>Where the night time carer sleeps at the property for a minimum of eight hours and will expect to get a reasonable night’s sleep but is on hand if required. This option will be best for you if you don’t require regular support but feel uncomfortable when left alone throughout the night. Anxieties that occur from being left alone throughout the night can soon be improved if you have a professional carer there to support you if needed. Sleep deprivation could worsen your condition, so having that extra night care at home can help you to remain healthy and happy. If the carer has to get up more than twice during the night, then it will be considered a waking night. Night care from Gable Healthcare is a flexible process and if you find that you or your loved one is waking more frequently throughout the night, your night time carer will adjust to your needs.</p><p>
                                </div>
                            </div>
                        </div>
                        <div class="panel sc_accordion_item">
                            <div class="" role="tab" id="headingTwo-1">
                                <h5 class="sc_accordion_title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTwo-1" aria-expanded="false" aria-controls="collapseTwo-1">
                                        Sitting Service
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo-1">
                                <div class="sc_accordion_content">
                                    <p>We are also able to provide a sitting service. This is where a carer arrives at the property keeping the person requiring night care company for between 2 – 8 hours, allowing the regular carer/family member to take a break. This is a great option for customers who need reassurance.
 </p>
                                </div>
                            </div>
                        </div>
                         <div class="panel sc_accordion_item">
                            <div class="" role="tab" id="headingFive-1">
                                <h5 class="sc_accordion_title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFive-1" aria-expanded="false" aria-controls="collapseFive-1">
                                        What next?
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseFive-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive-1">
                                <div class="sc_accordion_content">
                                    <p>
                                        For more information on any of our services please call us on <b>033 0555 0033</b> or contact us via email: info@gablehealthcare.co.uk.
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>
                    
</div>