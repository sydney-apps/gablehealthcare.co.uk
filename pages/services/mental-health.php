<div class="col-md-9 col-sm-8 content">
                    <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#servicesSlider" data-slide-to="0" class="active"></li>
   <!--                         <li data-target="#servicesSlider" data-slide-to="1"></li>
                            <li data-target="#servicesSlider" data-slide-to="2"></li>
       -->                 </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="/images/pages/intensive-care-service.png" alt="Slide 1">
                            </div>
<!--
                            <div class="item">
                                <img src="/images/Depositphotos_11349063_l.jpg" alt="Slide 2">
                            </div>

                            <div class="item">
                                <img src="/images/Depositphotos_11349063_l.jpg" alt="Slide 3">
                            </div>
-->
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#servicesSlider" role="button" data-slide="prev">
                            <span class="icon-angle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#servicesSlider" role="button" data-slide="next">
                            <span class="icon-angle-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <h5 class="margin-top-large margin-bottom-small">What is mental illness</h5>
                    <p>There are many types of mental illness and, depending on how mental illness is described, 10 or 25% of people living in the United Kingdom can be considered to have had a mental illness at some point in their lives. Like many other illnesses it is common and can occur at any time and happen to any one of us.
                        <br/>
                        Severe mental illness is a term used for longstanding conditions and effects only about 1% of the population. There are many types of mental illness but the easiest way of defining them are as either psychotic or neurotic. Most conditions fit into either of these categories.
</p>
                    <div class="sc_accordion sc_accordion_style_1 margin-top-large margin-bottom-large" id="accordion-1" role="tablist" aria-multiselectable="true">
                        <div class="panel sc_accordion_item">
                            <div class="" role="tab" id="headingOne-1">
                                <h5 class="sc_accordion_title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseOne-1" aria-expanded="true" aria-controls="collapseOne-1">
                                        Neurotic conditions  
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne-1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne-1">
                                <div class="sc_accordion_content">
                                    Neurotic conditions are related to ‘normal’ emotions and are the most common type of mental illness. Many of us feel depressed for example and whilst it occurs is unpleasant. However if you have clinical depression it is a far deeper experience than being ‘feeling depressed’. Having clinical depression is an illness that has a marked effect on your life, preventing the patients being able to work or look after themselves properly and in extreme cases lead to suicide. Other examples of neurotic illnesses are Phobias, Obsessive Compulsive Disorder and Anxiety.
                                </div>
                            </div>
                        </div>
                        <div class="panel sc_accordion_item">
                            <div class="" role="tab" id="headingTwo-1">
                                <h5 class="sc_accordion_title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTwo-1" aria-expanded="false" aria-controls="collapseTwo-1">
                                        Psycotic conditions
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo-1">
                                <div class="sc_accordion_content">
                                <p>These conditions are different from neuroses being unrelated to normal emotions. Psychosis is a word used to describe symptoms or experiences that happen together. These symptoms cause the patient to not experience reality like most people. Someone with psychotic symptoms may:
                                    </p>
                                    <ul>
                                        <li>Hear, smell feel or see things which others do not – hallucinations</li>
                                        <li>Have strange thoughts or beliefs that can make the person feel they are being persecuted or controlled – delusions</li>
                                        <li>Have muddled or blocked thinking – thought disorder</li>
                                        <li>Appear unusually excited or withdrawn and avoid contact with people</li>
                                        <li>Not recognise that they are unwell – lack of insight</li>
                                    </ul>
                                    <p>These symptoms can occur with a number of psychotic illnesses including schizophrenia. People suffering from Bipolar Disorder 
also known as Manic–Depression, and psychotic depression, which are mood disorders, can also experience these symptoms.
                                    </p>    
                                </div>
                            </div>
                        </div>
                        
                        <div class="panel sc_accordion_item">
                            <div class="" role="tab" id="headingThree-1">
                                <h5 class="sc_accordion_title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseThree-1" aria-expanded="false" aria-controls="collapseThree-1">
                                        Understanding Mental Health Jargon
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo-1">
                                <div class="sc_accordion_content">
                                    <p>
                                        
</br>Understanding Mental Health Jargon 	 
</br>Some of the language used in mental health can be confusing to anyone who isn't a trained mental health professional. In the glossary below you can find explanations of various terms used in mental health. Below the glossary is anAbbreviations Explained' section where you can find explanations of some of the abbreviations mental health professionals use.
</br><b>Glossary</b>
</br><b>Accident and Emergency (A&E)</b>
</br>A walk-in centre at hospitals for when urgent or immediate treatment is necessary.
</br><b>Acute</b>
</br>An acute illness is one that develops suddenly. Acute conditions may or may not be severe and they usually last for a short amount of time.
</br><b>Admission beds </b>
</br>NHS beds that are available for people in a crisis, when care cannot be provided in their own home.
</br><b>Advocate</b>
</br>An advocate is someone who helps to support a service user or carer through their contact with health services. 

</br><b>Allied Health Professionals (AHPs)</b>
</br>A range of health professionals that includes physiotherapists, occupational therapists, dieticians, art therapists, and speech and language therapists.
</br><b>Anti-psychotic medication</b>
</br>Medication used to treat psychosis. There are several different types of anti-psychotic medication.
</br><b>Assertive outreach</b>
</br>Assertive outreach refers to a way of delivering treatment. An Assertive Outreach Team actively take their service to people instead of people coming to the team. Care and support may be offered in the service user's home or in some other community setting. Care and support is offered at times suited to the service user rather than times suited to the team's convenience.
</br><b>Assessment </b>
</br>When someone is unwell, health care professionals meet with the person to talk to them and find out more about their symptoms so they can make a diagnosis and plan treatments. This is called an assessment. Family members should be involved in assessments, unless the person who is unwell says he or she does not want that.

</br><b>Caldicott guardian</b>
</br>The person within a Trust who has responsibility for policies on safeguarding the confidentiality of patient information. 

</br><b>Care pathways </b>
</br><b>This is the route someone who is unwell follows through health services. The path starts when someone first contacts health services</b>- through their GP or an accident and emergency department, for example. The path continues through diagnosis, treatment, and care.
</br><b>Care plan</b>
</br>Mental health professionals draw up a care plan with someone when they first start offering them support, after they have assessed what someone's needs are and what is the best package of help they can offer. People should be given a copy of their care plan and it should be reviewed regularly. Service users, and their families and carers, can be involved in the discussion of what the right care plan is.

</br><b>Care Programme Approach (CPA) </b>
</br>A way of assessing the health and social care needs of people with mental health problems, and coming up with a care plan that ensures people get the full help and support they need. 

</br><b>Carer</b>
</br>A friend or relative who voluntarily looks after someone who is ill, disabled, vulnerable, or frail. Carers can provide care part-time or full-time. 

</br><b>Challenging behaviour </b>
</br>Behaviour that puts the safety of the person or other people at risk, or that has a significant impact on the person's or other people's quality of life.
</br>Child and Adolescent Mental Health Services (CAMHS) 
</br>CAMHS provide individual and family work helping children and young people under the age of 18 who experience emotional difficulties or mental health problems

</br><b>Chronic condition </b>
</br>A condition that develops slowly and/or lasts a long time.

</br><b>Client</b>
</br>Someone who uses health services. Some people use the terms patient or service user instead. 

</br><b>Clinical governance </b>
</br>A system of steps and procedures through which NHS organisations are accountable for improving quality and safeguarding high standards to ensure that patients receive the highest possible quality of care
</br><b>Clinician </b>
</br>A health professional who is directly involved in the care and treatment of people. Examples include nurses, doctors, and therapists.
</br><b>Cognitive behavioural therapy (CBT) </b>
</br>This is a way of helping people to cope with stress and emotional difficulties by encouraging them to make the connections between how we think, how we feel, and how we behave. 

</br><b>Commissioning </b>
</br>The process by which commissioners decide which services to purchase for the local community and which provider to purchase them from. Most mental health services are commissioned by Primary Care Trusts.

</br><b>Community care</b>
</br>Care and support provided outside of a hospital. 

</br><b>Crisis</b>
</br>A mental health crisis is a sudden and intense period of severe mental distress.

</br><b>Day care</b>
</br>Communal care that is usually provided away from a service user's place of residence with carers present.
</br><b>Depot injections </b>
</br>Long acting medication often used where people are unable or unwilling to take tablets regularly.
</br><b>Dual diagnosis</b>
</br>When two or more problems or disorders affect a person at the same time.
</br><b>Early intervention service </b>
</br>A service for people experiencing their first episode of psychosis. Research suggests that early detection and treatment will significantly increase recovery.

</br><b>Forensic services </b>
</br>Services that provide support to offenders with mental health problems.
</br><b>Formal patient</b>
</br>A formal patient is a person who has been detained in hospital under a section of the Mental Health Act (1983).
</br><b>Foundation Trusts </b>
</br>NHS Foundation Trusts have been created to shift a certain amount of decision-making from central Government control to local organisations and communities. This should make Foundation Trusts more responsive to the needs and wishes of their local people.
</br><b>Functional mental health problems</b>
</br>A term for any mental illness in which there is no evidence of organic disturbance (as there is with dementia) even though physical performance is impaired.
</br><b>General practitioner (GP) </b>
</br>GPs are family doctors who provide general health services to a local community. They are usually based in a GP surgery or practice and are often the first place people go with a health concern.
</br><b>Holistic </b>
</br><b>Taking into consideration as much about a person as possible in the treatment of an illness</b>- this includes their physical, emotional, psychological, spiritual, and social needs.
</br><b>Independent sector </b>
</br>Voluntary, charitable, and private care providers.
</br><b>Inpatient services </b>
</br>Services where the service user is accommodated on a ward and receives treatment there from specialist health professionals.
</br><b>Integrated Services</b>
</br>Health and social care professionals (such as social workers) working together in one team to provide a comprehensive range of support.
</br><b>Intervention</b>
</br>An intervention' describes any treatment or support that is given to someone who is unwell. An intervention could be medication, a talking therapy, or an hour spent with a volunteer.
</br>Low secure mental health services 
</br>Intensive rehabilitation services for offenders who have mental health problems.
</br><b>Mental health </b>
</br>Someone's ability to manage and cope with the stress and challenges of life, and to manage any diagnosed mental health problems as part of leading their normal everyday life.
</br><b>Mental Health Act (1983)</b>
</br>The Mental Health Act is a law that allows for the compulsory detention of people in hospital for assessment and treatment of a mental illness.

</br><b>Mental health trust</b>
</br>A mental health trust provides treatment, care and advice to people who have mental health problems. The services may be provided from a hospital or in the community.

</br><b>Multi-disciplinary team</b>
</br>A team made up of a range of both health and social care workers combining their skills to help people. 

</br>National institute for clinical excellence (NICE)
</br>An organisation responsible for providing guidance on best practice and the prevention and treatment of ill health.
</br><b>National Service Frameworks (NSF)</b>
</br>A set of quality standards for services issued by the Department of Health.

</br><b>Non-executive director (Ned)</b>
</br>A member of the Trust's board who represents community interest and uses their knowledge and expertise to help improve trust services.

</br>Non-executive directors have a responsibility to ensure the trust is fully accountable to the public for the services it provides and the public funds it uses.
</br><b>Older Adults</b>
</br><b>Adults aged over 65.</b>
</br><b>Organic illness</b>
</br>Illness affecting memory and other functions that is often associated with old age. Dementia, including Alzheimer's Disease, is an organic mental illness.
</br><b>Out-patient Services</b>
</br>Services provided to someone who comes to a hospital for treatment, consultation, and advice but who does not require a stay in the hospital.
</br><b>Overview and scrutiny committee </b>
</br>A County Council committee that is responsible for looking at the details and implications of decisions about changes to health services, and the processes used to reach these decisions.
</br><b>Patient</b>
</br>Someone who uses health services. Some people use the terms service user or client instead.
</br>Patient Advice and Liaison Service (PALS)
</br>All NHS Trusts have a Patient Advice and Liaison Service. They provide support, advice, and information to service users and their families. They can also tell you how to complain about a service, and can explain the Trust's complaints procedures.

</br><b>Pharmacist</b>
</br>Specialist health professionals who make, dispense, and sell medicines.

</br><b>Primary care</b>
</br>Health services that are the first point of contact for people with health concerns. Examples include GP surgeries, pharmacies, the local dentists, and opticians.
</br><b>Primary Care Trust (PCT)</b>
</br>Primary Care Trusts are responsible for planning and securing health services in their local area.
</br>Psychiatric intensive care unit (PICU) 
</br>A locked ward in a hospital where some people detained under the Mental Health Act may stay. They stay in the unit because they have been assessed as being at risk to themselves or others on an open acute inpatient care ward.
</br><b>Psycho-educational groups</b>
</br>Group work, using psychological therapy techniques, that address mental and emotional problems such as anxiety, depression, trauma, and severe stress.
</br><b>Psychosis </b>
</br>A mental state in which someone may show confused thinking, think that people are watching them, and see, feel, or hear things that other people cannot.
</br><b>Rehabilitation</b>
</br>A programme of therapy that aims to restore someone's independence and confidence and reduce disability.

</br><b>Residential and nursing homes</b>
</br>Residential and nursing homes provide round the clock care for vulnerable adults and older adults who can no longer be supported in their own homes. Homes may be run by local councils or independent organisations.
</br><b>Respite care</b>
</br>An opportunity for a carer to have a break. 

</br><b>Secondary Mental Health Services</b>
</br>Specialist mental health services usually provided by a Mental Health Trust. Services include support and treatment in the community as well as in hospitals.
</br><b>Sectioning</b>
</br>When someone is sectioned it means they are compulsorily admitted to hospital.
</br><b>Service level agreements (SLAs) </b>
</br>Internal NHS agreements between Primary Care Trusts and other NHS Trusts on the services to be provided to the local population, what their standards will be, and how monitoring will take place.
</br><b>Service user</b>
</br>This is someone who uses health services. Some people use the terms patient or client instead. 

</br><b>Social care </b>
</br>Social care describes services and support that help people live their lives as fully as possible, whereas health care focuses on treating an illness. Both types of care are offered as a combined package of support to people with mental health problems.
</br><b>Social inclusion </b>
</br>Ensuring that vulnerable or disadvantaged groups are able to access all of the activities and benefits available to anyone living in the community. 

</br><b>Stakeholder </b>
</br>Anybody who has an interest in an organisation, its activities, and its achievements.
</br><b>Stigma</b>
</br>Society's negative attitude to people, often caused by lack of understanding. Stigma can be a problem for people who experience mental ill health.
</br><b>Supervised Community Treatment</b>
</br>When someone detained under the Mental Health Act for treatment is discharged from hospital, they can be placed on Supervised Community Treatment.' This means they can return home but continue to be treated without their consent.
</br><b>Supplementary prescribing </b>
</br>A partnership between a doctor, a service user, and a nurse or Allied Health Professional (AHP). Under the partnership the nurse or AHP can make adjustments to someone's medication based on an agreed care plan.
</br><b>Abbreviations Explained</b>
</br><b>A&E</b>- Accident and Emergency
</br><b>ACF</b>- Acute Care Forum
</br><b>AHP</b>- Allied Healthcare Professional
</br><b>AMHP</b>- Approved Mental Health Practitioner
</br><b>AOA</b>- Adult and Older Adult (Services)
</br><b>AoG</b>- Assembly of Governors
</br><b>AOT</b>- Assertive Outreach Team
</br><b>ASD</b>- Autistic Spectrum Disorder
</br><b>ASW</b>- Approved Social Worker
</br><b>BME</b>- Black and Minority Ethnic
</br><b>BoD</b>- Board of Directors
</br><b>CAMHS</b>- Child and Adolescent Mental Health Services
</br><b>CAT</b>- Change Agent Team
</br><b>CBT</b>- Cognitive Behavioural Therapy
</br><b>CDW</b>- Community Development Worker
</br><b>CEO</b>- Chief Executive Officer
</br><b>CHAI</b>- Commission for Healthcare Audit Inspection
</br><b>CMHT</b>- Community Mental Health Team
</br><b>CNST</b>- Clinical Negligence Scheme for Trust
</br><b>CPA</b>- Care Programme Approach
</br><b>CPN</b>- Community Psychiatric Nurse
</br><b>CRHT</b>- Crisis Resolution and Home Treatment
</br><b>CSCI</b>- Commission for Social Care Inspection
</br><b>CQC</b>- Care Quality Commission
</br><b>CQUIN</b>- Commissioning for Quality and Innovation
</br><b>DAAT</b>- Drug and Alcohol Action Team
</br><b>DDA</b>- Disability Discrimination Act
</br><b>DNA</b>- Did Not Attend
</br><b>DoH</b>- Department of Health
</br><b>DSPD</b>- Dangerous and Severe Personality Disorder
</br><b>DTC</b>- Day Treatment Centre
</br><b>ECT</b>- Electro Convulsive Therapy
</br><b>ED</b>- Executive Directors
</br><b>EDS</b>- Eating Disorder Service
</br><b>EIS</b>- Early Intervention Service
</br><b>FT</b>- Foundation Trust
</br><b>FTN</b>- Foundation Trust Network
</br><b>GP</b>- General Practitioner
</br><b>HAZ</b>- Health Action Zone
</br><b>HCJ</b>- Health and Criminal Justice
</br><b>HDRU</b>- High Dependency Rehabilitation Unit
</br><b>HNA</b>- Health Needs Assessment
</br><b>HR</b>- Human Resources
</br><b>IAPT</b>- Improving Access to Psychological Therapies
</br><b>IC</b>- Infection Control
</br><b>ICN</b>- Integrated Care Network
</br><b>ICP</b>- Integrated Care Pathway
</br><b>IP</b>- In-patient
</br><b>LA</b>- Local Authority
</br><b>LD</b>- Learning Disabilities
</br><b>LINks</b>- Local Involvement Networks
</br><b>MCA</b>- Mental Capacity Act
</br><b>MDT</b>- Multi-Disciplinary Team
</br><b>MHA</b>- Mental Health Act
</br><b>NED</b>- Non-Executive Director
</br><b>NHS</b>- National Health Service
</br><b>NICE</b>- National Institute for Clinical Excellence in Health
</br><b>NPSA</b>- National Patient Safety Agency
</br><b>OBD</b>- Occupied Bed Days
</br><b>OP</b>- Out-patient
</br><b>OPMH</b>- Old People's Mental Health
</br><b>OT</b>- Occupational Therapist/Therapy
</br><b>PALS</b>- Patient Advice and Liaison Service
</br><b>PCT</b>- Primary Care Trust
</br><b>PCLT</b>- Primary Care Liaison Team
</br><b>PCS</b>- Professional Clinical Services
</br><b>PICU</b>- Psychiatric Intensive Care Unit
</br><b>PPI</b>- Patient and Public Involvement
</br><b>PSW</b>- Professional Social Worker
</br><b>RMN</b>- Registered Mental Nurse
</br><b>RNMH</b>- Registered Nurse in Mental Handicap
</br><b>SaLT</b>- Speech and Language Therapy
</br><b>SAP</b>- Single Assessment Process
</br><b>SHA</b>- Strategic Health Authority
</br><b>SMBC</b>- Sandwell Metropolitan Borough Council
</br><b>SMHFT</b>- Sandwell Mental Health and Social Care NHS Foundation Trust
</br><b>SMS</b>- Substance Misuse Services

                                    </p>    
                                </div>
                            </div>
                        </div>
                        
                    </div>
<p><b>Further Information</b><br/>
Useful websites for further information: 


<ul>
    <li>Royal College of Psychiatrists – <a href="ww.rcpsych.ac.uk">www.rcpsych.ac.uk</a></li>
    <li>Mind – <a href="www.mind.org.uk">www.mind.org.uk</a></li>
    <li>Centre for Mental Health – <a href="www.centreformentalhealth.org.uk">www.centreformentalhealth.org.uk</a></li>
    <li>Rethink – <a href="www.rethink.org">www.rethink.org</a></li>
</ul>
Visit our Choice and Medications website with explanations of common conditions and their treatment.<br/>
Learn more about psychosis by visiting our Early Intervention in Psychosis website.
</p>
                </div>