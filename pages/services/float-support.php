<div class="col-md-9 col-sm-8 content">
                    <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#servicesSlider" data-slide-to="0" class="active"></li>
 <!--                           <li data-target="#servicesSlider" data-slide-to="1"></li>
                            <li data-target="#servicesSlider" data-slide-to="2"></li> -->
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="/images/pages/support-living-debt-management.png" alt="Slide 1">
                            </div>
<!--
                            <div class="item">
                                <img src="/images/Depositphotos_11349063_l.jpg" alt="Slide 2">
                            </div>

                            <div class="item">
                                <img src="/images/Depositphotos_11349063_l.jpg" alt="Slide 3">
                            </div>
-->
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#servicesSlider" role="button" data-slide="prev">
                            <span class="icon-angle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#servicesSlider" role="button" data-slide="next">
                            <span class="icon-angle-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
<div class="item-pagenone">
<div class="page-header">
    <h2><a href="/index.php/our-services/home-care-2"> Float Support</a>
    </h2>
	</div>
<p>We provide two different types of floating support:<img src="/images/housing_s.png" alt="Housing Support" width="300" style="float: right; margin-left: 20px; margin-right: 20px;"></p>
<ul>
<li>housing related support to enable you to find and sustain a home&nbsp;</li>
</ul>
<p>Usually these services are short term services to enable individuals to gain skills and confidence to successfully sustain their accommodation. You can choose to end your support at any time, provided it is not a condition of your starter tenancy. <br> The service offers support to individuals with mental health needs and who would benefit from support with the following:</p>
<ul>
<li>Advice on managing finances and benefit entitlement</li>
<li>Support to reduce debt</li>
<li>Support you to access social, leisure and community activities</li>
<li>Finding training and educational opportunities</li>
<li>Support to manage general health, mental health, emotional and spiritual health and wellbeing</li>
<li>Signposting to appropriate services and support to access services</li>
<li>Supporting you to attain any assistive technology/aids and adaptations you may require.</li>
<li>Finding and moving on to other accommodation</li>
<li>Setting up a home, maintaining your tenancy and maintaining safety</li>
<li>Support to develop confidence</li>
<li>Support to help you gain control over your life&nbsp;</li>
</ul>
<p>Floating Drug and Alcohol Support Service <br> The Floating Drug and Alcohol Support Service offers specialist housing related support to people who experience difficulties with drugs or alcohol and are having problems sustaining their tenancy or home. <br> Staff have been fully trained and have a wealth of experience and understanding of these issues.<br> We can support people:</p>
<ul>
<li>who risk losing their home because of drugs or alcohol</li>
<li>who are aged 16 years old and over</li>
</ul>
<p>We can help with advice and practical support with:</p>
<ul>
<li>setting up home &nbsp;</li>
<li>housing and tenancy issues</li>
<li>finance, debt, welfare benefits and budgeting</li>
<li>personal safety and security</li>
<li>getting help and support from other services &nbsp;</li>
<li>supporting you to manage your drug or</li>
<li>alcohol issues</li>
<li>volunteering, work or college</li>
<li>acting as an advocate on your behalf</li>
</ul>
<p>We do this by:</p>
<ul>
<li>&nbsp;meeting with you in your home</li>
<li>discussing what type of support would help you &nbsp;</li>
<li>agreeing a set of personal goals &nbsp;</li>
<li>planning regular support sessions, actions and activities with you</li>
</ul>
<p>For further information call, or for a referral form please call: <br><strong>Tel</strong>&nbsp; &nbsp; &nbsp; &nbsp;: 0330 555 0033 (Monday-Friday, 9am-5pm) or <br><strong>E-mail &nbsp;</strong>: 
 <a href="mailto:admin@gablehealthcare.co.uk">admin@gablehealthcare.co.uk</a>
<span style="display: none;">This email address is being protected from spambots. You need JavaScript enabled to view it.
</span> <br><br></p>
	
								 </div></p>
                </div>