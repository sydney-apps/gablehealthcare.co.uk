<?php ?>
<div class="col-md-9 col-sm-8 content">
                    <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#servicesSlider" data-slide-to="0" class="active"></li>
                            <li data-target="#servicesSlider" data-slide-to="1"></li>
                            <li data-target="#servicesSlider" data-slide-to="2"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="/images/pages/live-in-care.png" alt="Slide 1">
                            </div>

                            <div class="item">
                                <img src="/images/pages/support-living-debt-management.png" alt="Slide 2">
                            </div>

                            <div class="item">
                                <img src="/images/pages/supported-living.png" alt="Slide 3">
                            </div>

                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#servicesSlider" role="button" data-slide="prev">
                            <span class="icon-angle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#servicesSlider" role="button" data-slide="next">
                            <span class="icon-angle-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <h5 class="margin-top-large margin-bottom-small">What we provide</h5>
                    <p>As part of our supported living services, we help our clients gain the confidence and life skills they need to realise their potential and build the foundations for independent living. <br/>
                        <br />
                        We do this by providing a safe and secure environment, where the support is tailored to individual needs.<br /><br />
                        <br />
                        We provide:
                    <ul>
                        <li>Specialist quality accommodation </li>
                        <li>Floating support on behalf of external partners and for clients in their own homes </li>                    
                        <li>Move-on accommodation </li>
                        <li>Support in your accommodation</li>
                        <li>Support services for thousands of disadvantaged and vulnerable people.</li>
                    </ul>
</p>
                    <div class="sc_accordion sc_accordion_style_1 margin-top-large margin-bottom-large" id="accordion-1" role="tablist" aria-multiselectable="true">
                        <div class="panel sc_accordion_item">
                            <div class="" role="tab" id="headingOne-1">
                                <h5 class="sc_accordion_title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseOne-1" aria-expanded="true" aria-controls="collapseOne-1">
                                        Supported Living
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne-1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne-1">
                                <div class="sc_accordion_content">
                                    <p>Supported Living is all about providing high quality support and housing services to people who need support because of illness, a disability or because they are experiencing a crisis in their lives.</p><p>
                                        We offer supported living services to enable people to live independently and achieve their personal goals – whether that is to get a job, have their own home or make new friends.</p><p>
                                        We work closely with local authorities and other agencies to design and deliver a range of specialist services, such as move-on options for homeless people, day services, housing for people with learning disabilities, or self-contained flats and floating support services for women escaping domestic violence.</p><p>
                                </div>
                            </div>
                        </div>
                        <div class="panel sc_accordion_item">
                            <div class="" role="tab" id="headingTwo-1">
                                <h5 class="sc_accordion_title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTwo-1" aria-expanded="false" aria-controls="collapseTwo-1">
                                        Independent living
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo-1">
                                <div class="sc_accordion_content">
                                    <p>Independent living is a very different way to get the care and support you need in your own home.</p>
                                    <p>This means you could remain independent in a ‘home for life’, with the peace of mind that our dedicated staff are on hand 24-hours a day to provide you with as little or as many home care services as you require.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel sc_accordion_item">
                            <div class="" role="tab" id="headingThree-1">
                                <h5 class="sc_accordion_title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseThree-1" aria-expanded="false" aria-controls="collapseThree-1">
                                        Floating support
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree-1">
                                <div class="sc_accordion_content">
                                    <p>Floating Support helps people tackle all kinds of issues; from rent arrears, benefits issues and neighbour disputes, to activities that reduce social isolation. This support enables people with mental ill-health or other problems to sustain their tenancies, find the services they need and build a better life for themselves within their community</p>
                                    <p>
                                        Our Floating Support services are available to people living in accommodation provided by a housing association, local authority or other organisation.
                                    </p>

                                </div>
                            </div>
                        </div>
                        <div class="panel sc_accordion_item">
                            <div class="" role="tab" id="headingFour-1">
                                <h5 class="sc_accordion_title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFour-1" aria-expanded="false" aria-controls="collapseFour-1">
                                        Resettlement
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseFour-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour-1">
                                <div class="sc_accordion_content">
                                    <p>
                                        We have a variety of resettlement services, to support people leaving care or supported housing. The aim is to support their transition to independent living, where they no longer require long-term intensive support services.
                                    </p>

                                </div>
                            </div>
                        </div>
                         <div class="panel sc_accordion_item">
                            <div class="" role="tab" id="headingFive-1">
                                <h5 class="sc_accordion_title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFive-1" aria-expanded="false" aria-controls="collapseFive-1">
                                        Supported housing
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseFive-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive-1">
                                <div class="sc_accordion_content">
                                    <p>
                                        Our supported housing projects bring people together from different backgrounds, and sometimes with different needs, to share housing and support. This offers a positive environment where people’s unique qualities, strengths and abilities are properly recognised. For people who feel lonely or cut off from others, supported housing can provide a safe place to start making friends and building a better life.
                                    </p><p>
                                        Supported housing can be in a shared house or self-contained flats.
                                    <ul>
                                        <li><b>Shared houses </b><br/>
                                            A shared house is when a group of clients share a house together. Each person has their own room, but shares living areas with the other people that live in the house. 
                                        <li><b>Self-contained flats</b> <br/>
                                            Our self-contained flats can provide greater independence. Clients will have larger flats with a separate bedroom, living area and kitchen.
                                        </li>

                                    </ul>
                                    </p>

                                </div>
                            </div>
                        </div>
                        
                        
                         <div class="panel sc_accordion_item">
                            <div class="" role="tab" id="headingFive-1">
                                <h5 class="sc_accordion_title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFive-1" aria-expanded="false" aria-controls="collapseFive-1">
                                        Respite and Short Break Services
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseFive-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive-1">
                                <div class="sc_accordion_content">
                                    <p><strong>Gable Healthcare provides high quality, respectful respite and short break services which deliver a high level of service user and carer satisfaction and offer purchasers value for money. Our services provide a mix of 24 hour personal care and more general support promoting independence and ensuring personal dignity and quality of life.</strong></p>
<p><br> We provide services to older adults, adults with a learning disability and/or mental health need, to give their family carers periods of respite. We offer an advanced reservation system so carers can book breaks at times and dates to suit them, as well as a ‘last minute’ reservation system, where possible, to provide support in times of crisis or simply to allow spontaneity for families and carers.&nbsp;Gable Healthcare has experience of supporting people with different levels of need, from;</p>
    <ul>
        <li>mild learning disabilities to more complex needs, i.e., people with Autism, physical, sensory and communication issues,</li>
        <li>Mental health or Dementia</li>
        <li>challenging behaviours, specific health care needs and dual diagnosis.</li>
        <li>Older people</li>
    </ul>
    <p><br> The level of support given varies from person to person and we deliver support that is fully needs-led, with the service user in control of all the decisions which affect them and their lives.<br><br> We feel it is important that service users, families and carers have confidence in the quality of our services. Before a service user comes into our service, we organise meetings with them and their families or carers to build their confidence and trust in Gable Healthcare and to get a clear picture of the needs of the service user. We promote our services as a reliable and experienced care provider and encourage families and carers to get involved in our monitoring and quality assurance activities. <br><br>We offer open and informal services where family members feel free to approach us and know they can really get involved.<br><br> Service users and family carers work with staff to develop respite Support Plans, as well as Personalised Care Plan, which state the key goals and objectives the service users want to achieve whilst staying within the service. These plans are adjusted over time, particularly where service users frequently access the service and become well-known to staff.<br><br> Gable Healthcare offers service users and their family carers a high level of flexibility and choice across all areas of the service provided. Our services meet service users’ individual basic needs and we support them to take up a variety of activities and opportunities for development, based on their interests. We put the service user and their family at the centre of all decision making. <br><br><br></p>

                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                    
                </div>