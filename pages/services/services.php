<?php ?>
<div class="col-md-9 col-sm-8 content">
                    <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#servicesSlider" data-slide-to="0" class="active"></li>
                            <!-- <li data-target="#servicesSlider" data-slide-to="1"></li>
                            <li data-target="#servicesSlider" data-slide-to="2"></li> -->
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="/images/pages/supported-living.png" alt="Slide 1">
                            </div>
<!--
                            <div class="item">
                                <img src="images/Depositphotos_11349063_l.jpg" alt="Slide 2">
                            </div>

                            <div class="item">
                                <img src="images/Depositphotos_11349063_l.jpg" alt="Slide 3">
                            </div>

                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#servicesSlider" role="button" data-slide="prev">
                            <span class="icon-angle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#servicesSlider" role="button" data-slide="next">
                            <span class="icon-angle-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <h5 class="margin-top-large margin-bottom-small">Introduction to our services</h5>

                    <div class="category-desc clearfix">
							<p>
                                                            Gable Healthcare provides high quality care and support services for older people, people with learning disabilities, mental health needs and complex needs. <strong>Our principal areas of activity are:</strong></p>
<ul>
<li>Floating Support and Supporting People Services</li>
<li>Community &amp; Outreach Support</li>
<li>Home Care Services</li>
<li>Supported Housing</li>
<li>Supported Living</li>
<li>Residential Rehabilitation Services</li>
<li>Day Services and Activities</li>
<li>Extra Care Services in Sheltered Housing</li>
<li>BME (Black and Minority Ethnic) Services</li>
<li>Referral and Advice Services&nbsp;</li>
</ul>
<p>Gable Healthcare works with Social Services, Community Mental Health Teams (CMHT) and Supporting People within leading Local Authorities across England. We provide:</p>
<ul>
<li>Supported Housing: High quality supported housing.</li>
<li>Domiciliary Services: For service users that require personal care in their own home and community.</li>
<li>Personalised Budgets: Supporting service users to manage personal budgets either through direct payments or personal budgets managed by local authorities.</li>
</ul>
<p>Our service users are individuals who are:</p>
<ul>
<li>Aged between18-70 with recognised mental health needs or learning disabilities</li>
<li>Ready to be discharged from psychiatric wards</li>
<li>Have been private referrals from GPs</li>
<li>Recovering from substance and alcohol misuse&nbsp;</li>
</ul>
<p>
    To prepare our service users towards independent living for the future, Gable Healthcare has a holistic approach which involves CPN, OT, social workers, psychiatrists, GPs, care workers, advocates and their extended families.<br><br> Specifically, the services we offer through qualified and experienced staff, cover intensive support high, medium and floating (low level) support needs, from 1 hour a day or a week to 24 hours a day.
<br><br>
</p>			
</div>
</div>