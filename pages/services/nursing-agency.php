<div class="col-md-9 col-sm-8 content">
<div class="item-pagenone">
			<div class="page-header">
                            <h2>
                                <a href="/index.php/our-services/social-domiciliary-care-main-menu"> Nursing Agency</a>
                            </h2>
                        </div>
    <p>We provide staffing solutions to Nursing and residential care homes, NHS Trusts, Learning disabilities centres and private hospitals for our clients.<br><br> Whatever your requirements, we'll meet them, from a single booking to year-long contract,<br><br> <img src="/images/nurses.jpg" alt=""></p>
<ul>
<li>For one shifts to cover staff shortfalls or absence.</li>
<li>To help staff a new unit whilst new, permanent staffs are recruited.</li>
<li>To provide emergency cover in busy periods. &nbsp;</li>
<li>On a temp to perm basis</li>
<li>As regular, on going support to full time teams</li>
<li>On a contract or SLA basis. &nbsp;</li>
<li>Consolidated booking and invoicing systems and special rates. Make us your First Call and we can offer you special discounts too.</li>
</ul>
<p><br> Bank management: With experience of managing nurse banks in private hospitals and nursing homes throughout the UK. <br><br> To discuss your staffing needs please contact us on:<br><br><strong>Tel</strong>&nbsp; &nbsp; &nbsp; &nbsp;: 0330 555 0033 <br><strong>Email </strong>&nbsp; : 
 <a href="mailto:gablehealthcare@gmail.com">gablehealthcare@gmail.com</a>
 <span style="display: none;">This email address is being protected from spambots. You need JavaScript enabled to view it.
 </span> <br><strong>Mob</strong>&nbsp; &nbsp; &nbsp;: <tel>07809428407</tel>(Out of hours line). <br><br></p>
 </div>
                </div>