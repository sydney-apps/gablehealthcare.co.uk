<div class="col-md-9 col-sm-8 content">
                    <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#servicesSlider" data-slide-to="0" class="active"></li>
   <!--                         <li data-target="#servicesSlider" data-slide-to="1"></li>
                            <li data-target="#servicesSlider" data-slide-to="2"></li>
       -->                 </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="/images/pages/supported-living.png" alt="Slide 1">
                            </div>
<!--
                            <div class="item">
                                <img src="/images/Depositphotos_11349063_l.jpg" alt="Slide 2">
                            </div>

                            <div class="item">
                                <img src="/images/Depositphotos_11349063_l.jpg" alt="Slide 3">
                            </div>
-->
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#servicesSlider" role="button" data-slide="prev">
                            <span class="icon-angle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#servicesSlider" role="button" data-slide="next">
                            <span class="icon-angle-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
<div class="item-pagenone">
    <div class="page-header">
	<h2>
        	Live in care
        </h2>
    </div>
    <p>Gable Healthcare offers full time live-in care for people who do not want to leave their home life or pets and value their independence.<br><br> What is live-in care?<br> Live-in care is an affordable alternative to moving into a care home. People continue to live at home but with full time help always on hand.<br><br> Live-in care offers:</p>
<ul>
<li>Full time care provided by a live-in carer who lives in the home 24 hours a day, 7 days a week</li>
<li>A care plan tailored to individual needs</li>
<li>Help with personal care and household tasks</li>
<li>Care, support and companionship&nbsp;</li>
</ul>
<p>Benefits of live-in care<br> Benefits include:</p>
<ul>
<li>Peace of mind that care is always on hand &nbsp;</li>
<li>High degree of personalised support</li>
<li>Independence</li>
<li>Minimal disruption to daily life</li>
<li>No upheaval or stress caused by moving. Who could benefit from live-in care?</li>
</ul>
<p>We provide live-in care for people with a range of conditions including: </p>
<ul>
<li>Dementia</li>
<li>Multiple Sclerosis</li>
<li>Parkinson’s</li>
<li>Alzheimer’s</li>
<li>Strokes.&nbsp;</li>
</ul>
<p>For further information, please contact our experienced staff to see if we can support your requirement. <br><br></p>
</div>	
</div>