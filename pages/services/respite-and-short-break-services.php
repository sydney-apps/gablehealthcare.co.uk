<div class="col-md-9 col-sm-8 content">
                    <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#servicesSlider" data-slide-to="0" class="active"></li>
<!--                            <li data-target="#servicesSlider" data-slide-to="1"></li>
                            <li data-target="#servicesSlider" data-slide-to="2"></li>
    -->                    </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="/images/pages/intensive-care-service.png" alt="Slide 1">
                            </div>
<!--
                            <div class="item">
                                <img src="/images/Depositphotos_11349063_l.jpg" alt="Slide 2">
                            </div>

                            <div class="item">
                                <img src="/images/Depositphotos_11349063_l.jpg" alt="Slide 3">
                            </div>
-->
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#servicesSlider" role="button" data-slide="prev">
                            <span class="icon-angle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#servicesSlider" role="button" data-slide="next">
                            <span class="icon-angle-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
<div class="item-pagenone">
    <div class="page-header">
		<h2>
                    Respite and Short Break Services
                </h2>
	</div>
    
    <p><strong>Gable Healthcare provides high quality, respectful respite and short break services which deliver a high level of service user and carer satisfaction and offer purchasers value for money. Our services provide a mix of 24 hour personal care and more general support promoting independence and ensuring personal dignity and quality of life.</strong></p>
<p><br> We provide services to older adults, adults with a learning disability and/or mental health need, to give their family carers periods of respite. We offer an advanced reservation system so carers can book breaks at times and dates to suit them, as well as a ‘last minute’ reservation system, where possible, to provide support in times of crisis or simply to allow spontaneity for families and carers.&nbsp;Gable Healthcare has experience of supporting people with different levels of need, from;</p>
    <ul>
        <li>mild learning disabilities to more complex needs, i.e., people with Autism, physical, sensory and communication issues,</li>
        <li>Mental health or Dementia</li>
        <li>challenging behaviours, specific health care needs and dual diagnosis.</li>
        <li>Older people</li>
    </ul>
    <p><br> The level of support given varies from person to person and we deliver support that is fully needs-led, with the service user in control of all the decisions which affect them and their lives.<br><br> We feel it is important that service users, families and carers have confidence in the quality of our services. Before a service user comes into our service, we organise meetings with them and their families or carers to build their confidence and trust in Gable Healthcare and to get a clear picture of the needs of the service user. We promote our services as a reliable and experienced care provider and encourage families and carers to get involved in our monitoring and quality assurance activities. <br><br>We offer open and informal services where family members feel free to approach us and know they can really get involved.<br><br> Service users and family carers work with staff to develop respite Support Plans, as well as Personalised Care Plan, which state the key goals and objectives the service users want to achieve whilst staying within the service. These plans are adjusted over time, particularly where service users frequently access the service and become well-known to staff.<br><br> Gable Healthcare offers service users and their family carers a high level of flexibility and choice across all areas of the service provided. Our services meet service users’ individual basic needs and we support them to take up a variety of activities and opportunities for development, based on their interests. We put the service user and their family at the centre of all decision making. <br><br><br></p>
</div>
</div>