<?php
    include ('includes/header.php');
?>
<section>
<section id="gellery" class="container page masonry">
<div class="row">
<div class="col-sm-12 col-md-12">
<div class="content">
<div id="isotope" class="masonry grid gallery five_columns isotope no_padding portfolioWrap partners">
    <div class="isotopeElement">
        <article class="post_item post_item_masonry">
            <h5 class="post_title"><a href="#"><span class="post_icon icon-book-2"></span>Federation of Small Businesses</a>
            </h5>
            <div class="post_featured">
                <div class="post_thumb"
                    data-image="Depositphotos_5724724_original.jpg"
                    data-title="Federation of Small Businesses">
                    <img class="wp-post-image"
                                 alt="Missing Teeth Predict Strokes"
                                 src="/images/Depositphotos_5724724_original.jpg">
                    <a class=""
                        href="images/Depositphotos_5724724_original.jpg"
                        title="Federation of Small Businesses" rel="magnific">
                        <span class="grid_hover_icon icon-search"></span>
                    </a>
                    <a title="Federation of Small Businesses" href="http://www.fsb.org.uk">
                        <span class="grid_hover_icon icon-link"></span>
                    </a>
                    <div class="hover_shadow"></div>
                </div>
            </div>
        </article>
    </div>
    <div class="isotopeElement">
        <article class="post_item post_item_masonry">
            <h5 class="post_title"><a href="#"><span class="post_icon icon-book-2"></span>NHS</a>
            </h5>
            <div class="post_featured">
                <div class="post_thumb"
                     data-image="Depositphotos_9069855_original.jpg"
                     data-title="NHS">
                    <img class="wp-post-image"
                         alt="NHS"
                         src="images/Depositphotos_9069855_original.jpg">
                    <a class=""
                       href="images/Depositphotos_9069855_original.jpg"
                       title="NHS" rel="magnific">
                        <span class="grid_hover_icon icon-search"></span>
                    </a>
                    <a title="CNHS" href="http://nhs.gov.uk">
                        <span class="grid_hover_icon icon-link"></span>
                    </a>
                    <div class="hover_shadow"></div>
                </div>
            </div>
        </article>
    </div>
    <div class="isotopeElement">
        <article class="post_item post_item_masonry">
            <h5 class="post_title"><a href="#"><span class="post_icon icon-book-2"></span>Investors In People</a>
            </h5>
            <div class="post_featured">
                <div class="post_thumb"
                     data-image="Depositphotos_10408093_original.jpg"
                     data-title="nvestors In People">
                    <img class="wp-post-image"
                         alt="nvestors In People"
                         src="images/Depositphotos_10408093_original.jpg">
                    <a class=""
                       href="images/Depositphotos_10408093_original.jpg"
                       title="Hnvestors In People" rel="magnific">
                        <span class="grid_hover_icon icon-search"></span>
                    </a>
                    <a title="Cnvestors In People" href="https://www.investorsinpeople.com">
                        <span class="grid_hover_icon icon-link"></span>
                    </a>
                    <div class="hover_shadow"></div>
                </div>
            </div>
        </article>
    </div>
    <div class="isotopeElement">
        <article class="post_item post_item_masonry">
            <h5 class="post_title"><a href="#"><span class="post_icon icon-book-2"></span>Royal Kingstone</a>
            </h5>
            <div class="post_featured">
                <div class="post_thumb"
                     data-image="Depositphotos_9410241_original.jpg"
                     data-title="Royal Kingstone">
                    <img class="wp-post-image"
                         alt="Royal Kingstone"
                         src="images/Depositphotos_9410241_original.jpg">
                    <a class=""
                       href="images/Depositphotos_9410241_original.jpg"
                       title="Royal Kingstone" rel="magnific">
                        <span class="grid_hover_icon icon-search"></span>
                    </a>
                    <a title="Royal Kingstone" href="https://www.kingston.gov.uk">
                        <span class="grid_hover_icon icon-link"></span>
                    </a>
                    <div class="hover_shadow"></div>
                </div>
            </div>
        </article>
    </div>
    <div class="isotopeElement">
        <article class="post_item post_item_masonry">
            <h5 class="post_title"><a href="#"><span class="post_icon icon-book-2"></span>UKHCA</a>
            </h5>
            <div class="post_featured">
                <div class="post_thumb"
                     data-image="ukhca.jpg"
                     data-title="UKhA">
                    <img class="wp-post-image"
                         alt="RUKHCA"
                         src="images/clients-partners/ukhca.jpg">
                    <a class=""
                       href="images/clients-partners/ukhca.jpg"
                       title="UKHCA" rel="magnific">
                        <span class="grid_hover_icon icon-search"></span>
                    </a>
                    <a title="UKHCA" href="#">
                        <span class="grid_hover_icon icon-link"></span>
                    </a>
                    <div class="hover_shadow"></div>
                </div>
            </div>
        </article>
    </div>
    <div class="isotopeElement">
        <article class="post_item post_item_masonry">
            <h5 class="post_title"><a href="#"><span class="post_icon icon-book-2"></span>Care Quality Commission</a>
            </h5>
            <div class="post_featured">
                <div class="post_thumb"
                     data-image="cqc.jpg"
                     data-title="Care Quality Commission">
                    <img class="wp-post-image"
                         alt="Care Quality Commission"
                         src="images/clients-partners/cqc.jpg">
                    <a class=""
                       href="images/clients-partners/cqc.jpg"
                       title="Care Quality Commission" rel="magnific">
                        <span class="grid_hover_icon icon-search"></span>
                    </a>
                    <a title="Care Quality Commission" href="https://www.cqc.gov.uk">
                        <span class="grid_hover_icon icon-link"></span>
                    </a>
                    <div class="hover_shadow"></div>
                </div>
            </div>
        </article>
    </div>
</div>

</div>
</div>
</div>
</section>
</section>
    
        <?php include 'includes/footer.php'; ?>
