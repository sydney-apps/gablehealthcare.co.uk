<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <title>Gable Healthcare : Page not found</title>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" type="image/png" href="favicon.ico">
    <link rel="shortcut icon" type="image/png" href="http://eg.com/favicon.png"/>
    <link href="fonts/fontello/css/fontello.css" rel="stylesheet" type="text/css" media="all"/>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/flexslider.css" type="text/css">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/core.portfolio.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flexslider.css" type="text/css">
    <link href="css/styles.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/vendor/html5shiv.min.js"></script>
        <script src="js/vendor/respond.min.js"></script>
    <![endif]-->

</head>
<body>

    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

 <?php 
    include ('includes/header.php');
 ?>

        <section class="heading breadcrumb">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <h1 class="page_title">404</h1>
                    </div>
                    <div class="col-xs-6">
                        <div class="breadcrumbs text-right">
                            <a class="breadcrumbs_item home" href="index.html">Home</a><span
                                class="breadcrumbs_delimiter icon-right-open"></span><span class="breadcrumbs_item current">404</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="p404_section" class="light_section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 text-right">
                        <img src="images/404_dark.jpg" alt="">
                    </div>
                    <div class="col-sm-6">
                        <h2 class="page_title">We are <span>Sorry!</span><br>
                            Your Page cannot be found!</h2>
                        <p class="page_description">Can't find what you need? Take a moment and do a search below<br>or start
                            from <a href="http://dentalux.ancorathemes.com">our homepage</a>.</p>

                        <div class="p404_searchform_container margin-top-large">
                            <form class="" role="search">
                                <input type="text" class="form-control pull-left" placeholder="" name="srch-term" id="srch-term">
                                <button class="btn btn-default" type="submit">Search</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </section>

<?php include 'includes/footer.php'; ?>
    </div>

    <div class="preloader">
        <div class="preloader_image"></div>
    </div>

    <!-- libraries -->
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <script src="js/vendor/jquery-1.11.3.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/vendor/jquery.appear.js"></script>

    <!-- superfish menu  -->
    <script src="js/vendor/superfish.js"></script>

    <!-- page scrolling -->
    <script src="js/vendor/jquery.ui.totop.js"></script>

    <!-- sliders, filters, carousels -->
    <script src='js/vendor/jquery.flexslider-min.js'></script>

    <!-- custom scripts -->
    <script src="js/custom/front.min.js"></script>
    <script src="js/custom/utils.min.js"></script>
    <script src="js/custom/shortcodes_init.min.js"></script>

</body>
</html>