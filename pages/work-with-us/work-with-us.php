<?php ?>
<div class="col-md-9 col-sm-8 content">
                    <div id="servicesSlider" class="sc_slider carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#servicesSlider" data-slide-to="0" class="active"></li>
                            <li data-target="#servicesSlider" data-slide-to="1"></li>
                            <li data-target="#servicesSlider" data-slide-to="2"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="/images/pages/carers.jpg" alt="Slide 1">
                            </div>
<!--
                            <div class="item">
                                <img src="/images/pages/our-staff.png" alt="Slide 2">
                            </div>

                            <div class="item">
                                <img src="images/Depositphotos_11349063_l.jpg" alt="Slide 3">
                            </div>
-->
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#servicesSlider" role="button" data-slide="prev">
                            <span class="icon-angle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#servicesSlider" role="button" data-slide="next">
                            <span class="icon-angle-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
    
                    <div class="category-desc clearfix">
                    <div class="item-pagenone">
			<div class="page-header">
                            <h2>
                                <a href="/index.php/work-with-us"> Work With Us</a>
                                
                            </h2>
                            
                        </div>
                        <p style="text-align: justify;">Gable Healthcare has a wide range of services that support older people, people with learning disabilities and associated complex needs, including mental health needs, physical and sensory impairments and challenging behaviour.&nbsp;<br><br> Our services are based in a wide variety of community settings, including supported living, domiciliary care, residential services, day services and respite throughout England. At Gable Healthcare we make a difference to the lives of the individuals who have chosen, often with help, to use our services.<br><br> Our main focus is on the recruitment of staff with high caliber staff and investing in their personal development. There are comprehensive mandatory and specialist training programmes in place which ensure service users only receive the highest quality of care and support. In addition to this, staffs are also supported to complete NVQs and there are many opportunities for personal progression. <br><br> <strong>Applications</strong><br><br> If you are interested in applying for a role within Gable Healthcare or would like more information, please contact us or send us your up-to-date CV.<br><br> 
                            
<a href="mailto:admin@gablehealthcare.co.uk">admin@gablehealthcare.co.uk</a>
 <span style="display: none;">This email address is being protected from spambots. You need JavaScript enabled to view it.
 
 </span><br> 
 <a href="mailto:gablehealthcare@gmail.com">gablehealthcare@gmail.com</a>
 </span><br><br> <strong><a href="/downloads/application-form.pdf" target="_blank" title="Application form">Application Form Download </a></strong><br><br><br></p>
<p>&nbsp;</p>
	
						 </div>
                    </div>
</div>