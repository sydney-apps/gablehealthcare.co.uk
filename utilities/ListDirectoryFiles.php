<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ListDirectoryFiles
 *
 * @author Sydney
 */
abstract class ListDirectoryFiles {
    //put your code here
    private $fileList;

    abstract public function listFileNames($path);
    
    abstract public function getFileList();
}
