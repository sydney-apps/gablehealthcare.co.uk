<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StringOperations
 *
 * @author Sydney
 */
final class StringOperations {
    
    
    private function __construct(){}

    public static function AreTheSame($StringToCompare, $StringToBeComparedTo)
    {
        return strnatcasecmp($StringToCompare, $StringToBeComparedTo);
    }
}
?>