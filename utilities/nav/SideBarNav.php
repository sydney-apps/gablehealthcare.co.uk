<?php

/**
 * Description of SideBarNav
 *
 * @author Sydney
 */
class SideBarNav {

    //put your code here
    private $dom;
    private $path;
    private $CSSClass;

    function __construct($doc) {
        $this->dom = new DOMDocument();
        $this->path = $_SERVER['DOCUMENT_ROOT'] ."/". $doc;
        $this->path = str_replace('\\', '\/', $this->path);
        $this->dom->loadHTMLFile(realpath($this->path));
    }

    /**
     * 
     * @param type $CSSClass CSS class to set for the page.
     * @param type $tag tage that has the name of page
     * @param type $currentPage current page name
     */
    function setCurrentNode($CSSClass, $tag, $currentPage) {
        $root = $this->dom->documentElement;
        $this->CSSClass = $CSSClass;
        $nodesToStyle = array();
        $links = $root->getElementsByTagName($tag);
        $pos = 0;
        $posOfSelected = 0;

        foreach ($links as $a) {
            $pos++;
            //if (strpos(strtolower($a->nodeValue), str_replace('-', ' ', $currentPage)) !== false) {
            $_MenuItemName = str_replace('-', ' ',(strtolower($a->nodeValue)));
            $_CurrentPageName = str_replace('-', ' ', $currentPage);
            $_IsSelectedMenuItem = strnatcasecmp($_MenuItemName, $_CurrentPageName);
            if( $_IsSelectedMenuItem === 0){
                $nodesToStyle[] = $a;
                $posOfSelected = $pos;
            }
        }
        
        $pos = 0;
        foreach ($links as $a) {
            $pos++;

            if ($pos === ($posOfSelected - 1)) {
                $a->parentNode->setAttribute('class', 'beforeSelected');
            } elseif ($pos === $posOfSelected) {
                $a->parentNode->setAttribute('class', $this->CSSClass);
            } elseif ($pos === ($posOfSelected + 1)) {
                $a->parentNode->setAttribute('class', 'afterSelected');
            }
        }

        // You delete the nodes
//        foreach ($nodesToStyle as $node) {
//            $node->parentNode->setAttribute('class', $this->CSSClass);
//        }
        echo $this->dom->saveXML();
    }

}
