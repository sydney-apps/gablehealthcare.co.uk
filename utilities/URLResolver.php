<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of URLResolver
 *
 * @author Sydney
 */


class URLResolver {

    //put your code here
    private $partsOfURL = array();
    private $filePath = "";
    private $pageFound = false;
    private $isHome = false;
    private $currentPage = '';
    private $parent = null;
    private $count = 0;
    private $breadCrump = array();

    /**
     * 
     * @param type $url
     */
    public function __construct($url) {
        $this->partsOfURL = explode('/', $url);
        $this->count = count($this->partsOfURL)-1;
        $this->setURL($url);
        $this->setParent();
        $this->setBreadCrump();
    }
    function getBreadCrump() {
        return $this->breadCrump;
    }

    function setBreadCrump() {
        foreach ($this->partsOfURL as $crump){
            if(!empty($crump) && null !== $crump){
                $this->breadCrump[count($this->breadCrump)] = $crump;
            }
        }
    }

        public function getFile() {
        return $this->filePath;
    }

    public function isHomePage() {
        return $this->isHome;
    }
    
    /**
     * Breaksdown the URL parts into individual segments and assigns the current
     * page name and sets the file location value.
     * @param type $url
     */
    public function setURL($url) {
        if (count($this->partsOfURL) === 2) {
            if ($this->partsOfURL[1] === "" || strpos($this->partsOfURL[1], 'index.php') !== false) {
                $this->filePath = "/pages/home.php";
                $this->isHome = true;
                $this->currentPage = 'Home';
            } elseif($this->parent === DOWNLOADS){
                $this->currentPage = '/'.DOWNLOADS.'/'.$this->partsOfURL[1].'.pdf';
            }else{
                $this->filePath = '/pages/'.$this->partsOfURL[1].".php";
                //echo $this->filePath;
                $this->currentPage = $this->partsOfURL[1];
            }
        } else {
            $this->filePath = "/pages/".$url.".php";
            //echo $this->filePath;
            $this->currentPage = $this->partsOfURL[$this->count];
        }
        
        
    }
    function getCount() {
        return $this->count;
    }

    function setParent() {
        if(count($this->partsOfURL)>0){
            if(($this->partsOfURL[1]) !== '' && (count($this->partsOfURL)>=2)){
                $this->parent = $this->partsOfURL[1];
            }  
            else {
                // Something else
                unset($this->parent);
            }            
        }
        //$this->parent = $parent;
    }
    
    function getParent() {
        if (isset($this->parent)){
            return $this->parent;
        }else {
            return "";
        }
    }
    function getParentName(){
        return ucfirst(str_replace('-', ' ', $this->getParent()));
    }
    
    function getParentFile(){
        return 'pages/'.$this->parent.'.php';
    }

    function getCurrentPage() {
        return $this->currentPage;
    }

    public function pageFound() {

        $filePath = $_SERVER['DOCUMENT_ROOT'].$this->filePath;

        if (file_exists($filePath)) {
            $this->pageFound = true;
        } else {
            $this->pageFound = false;
        }
        return $this->pageFound;
    }
    
    public function fileExists(){
        $filePath = $_SERVER['DOCUMENT_ROOT'].$this->filePath;
    }

}
