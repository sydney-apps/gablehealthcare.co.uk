<?php

$dom=new DOMDocument();
//$dom->load('nav.xml');

include 'utilities/StringOperations.php';

$path = str_replace('\\', '\/', NAV__FILE);
$dom->loadHTMLFile(realpath($path));

$root=$dom->documentElement; 
$nodesToStyle=array();
$links=$root->getElementsByTagName('a');
$__PageNameToCompare = str_replace('-', ' ', $URLResolver->getCurrentPage());

$__ParentPageNameToCompare = '';

if(null !== $URLResolver->getParent()){
    $__ParentPageNameToCompare = str_replace('-', ' ', $URLResolver->getParent());
}

foreach ($links as $a) {
    $_MenuItemName = strtolower($a->nodeValue);
    $_IsThisMenuItem = StringOperations::AreTheSame($_MenuItemName, $__ParentPageNameToCompare);
    
    if(StringOperations::AreTheSame($_MenuItemName, $__PageNameToCompare) === 0){
            $nodesToStyle[]=$a;
        }else if($_IsThisMenuItem  === 0){
            $nodesToStyle[]=$a;
        }
}

// You delete the nodes
foreach ($nodesToStyle as $node){ $node->parentNode->setAttribute('class','current-menu-item');}
       $elements = $dom->saveXML();
 echo $elements;
?>

