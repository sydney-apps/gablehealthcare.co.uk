<?php
/**
 * Description of library
 *
 * @author Sydney
 */
include_once 'utilities/StringOperations.php';
include_once 'library/Framework/IXMLReader.php';
include_once 'library/Framework/PHPMailer/PHPMailerClass.php';

class ContactForm {
//$dom->load('nav.xml');
    /**
     *  Fields
     */
    //<editor-fold>
    
    private $dom ;
    private $path = '';
    private $isvalidmail = false;
    private $message = '';
    private $MESSAGE_SEND = 'Message has been sent';
    private $MESSAGE_ERROR = 'An error has occurred.';
    private $sendstatus = '';
    private $HtmlForm;
    
    private $FormXMLDocument;
    private $FormFields;
    
    //</editor-fold>
   
    public function __construct() {
        
        $this->sendstatus = 'Contact us for free consultation';
        $this->FormXMLDocument = new ContactFormReader(CONTACT_FORM);
        if(isset($_REQUEST['issend'])){
            $this->isvalidmail = true;
            $this->ProcessForm();
        }
        $this->HtmlForm = $this->FormXMLDocument->SaveXML();
    }

    public function ProcessForm(){
        
        $this->FormFields = new FormElementTypeProcessor($this->FormXMLDocument->GetOrderedFieldNames());
        $ElementGroups = $this->FormXMLDocument->GetListOfElementsByType();
        $this->FormFields->ValidateFields($ElementGroups);
        ($this->FormFields->IsMessageValid()) ? $this->SendMessage() : $this->SetErrorMessage();
    }
    /**
     *  :: Validation Methods ::
     */
    //<editor-fold>
    /**
     * 
     * @param string $param name of input
     * @param string $default placeholder value
     * @param string $required is it optional or required
     * @return boolean
     */
 
    //</editor-fold>
    
    public function ShowFormTitle() {
        echo $this->sendstatus;
    }
    
    public function DisplayForm(){
        echo $this->HtmlForm;
    }
    
    public function SendMessage(){
        $MessageBody = $this->FormFields->ComposeMessage();
        $Mailer = new PHPMailerClass();
        $senderName = $this->FormFields->GetValuesCollection()->getItem('name').' '. $this->FormFields->GetValuesCollection()->getItem('last-name');
        $sendEmail = $this->FormFields->GetValuesCollection()->getItem('email');
        if($Mailer->sendMail($sendEmail, $senderName, APPLICATION_RECIPIENT_EMAIL, WEBMAIL_CONTACT_FORM, $MessageBody)){
                $this->sendstatus = $this->MESSAGE_SEND;
            }else{
                $this->SetErrorMessage();
            }
    }
    
    private function SetErrorMessage(){
        $this->sendstatus = $this->MESSAGE_ERROR;
    }
    
}


/**
 * Validating different types of fields.
 * 
 */
class ValidateInput {
    
    /**
     * Validate email
     * @param string $email email address
     * @return boolean
     */
    static public function EMail($email){       
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
    
    /**
     * Validate phone number
     * @param numeric $phone
     */
    static public function PhoneNumber($phone){
        $phone = preg_replace('/[^0-9]/', '', $phone);
        return (strlen($phone) == 11);
    }
    
    /**
     * 
     * @param string $type
     * @param string/int $value
     */
    static public function OfType($type, $value){
        
        switch (true) {
            case ($type == EMAIL) :
                return self::EMail($value);
            break;
            case ($type == UK_PHONE) :
                return self::PhoneNumber($value);
            break;
            default :  
            return true;
            break;
    }
    
    }
}
?>