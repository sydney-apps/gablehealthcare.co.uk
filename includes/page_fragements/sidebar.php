
                <div class="col-md-3 col-sm-4 services_sidebar ">
                    <h3 class="margin-top-no">What we do</h3>
                    <aside class="widget_nav_menu">
                        <ul class="nav nav-pills nav-stacked services_menu">
                            <li role="presentation" class="active"><a
                                    href="#">Teeth Cleaning</a></li>
                            <li role="presentation"><a
                                    href="#">Radiographs</a></li>
                            <li role="presentation"><a
                                    href="#">Teeth Whitening</a></li>
                            <li role="presentation"><a
                                    href="#">Orthodonics</a></li>
                            <li role="presentation"><a
                                    href="#">Implants</a></li>
                            <li role="presentation"><a
                                    href="#">Cosmetic Works</a></li>
                        </ul>
                    </aside>
                    <aside class="widget widget_download_pricelist"><h5 class="widget_title">Downloads</h5>
                        <ul>
                            <li class="cat-item"><a href="#"><span class="sc_icon icon-file-word"></span>Application Form.doc</a></li>
                            <li class="cat-item"><a href="#"><span class="sc_icon icon-file-powerpoint"></span>Services Brochure.doc</a></li>
                            <li class="cat-item"><a href="#"><span class="sc_icon icon-file-pdf"></span>Services Picelist.pdf</a></li>
                        </ul>
                    </aside>
                    <aside class="widget widget_contacts"><h5 class="widget_title">Contacts</h5>
                        <ul>
                            <li class="cat-item address">ALFA House, Molesey Road, Walton-On-Thames, Surrey, KT12 3PD</li>
                            <li class="cat-item phone"> 0800 123 45 678</li>
                        </ul>
                    </aside>
                </div>