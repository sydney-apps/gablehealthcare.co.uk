<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]> <html class="no-js"> <!--<![endif]-->
<head>
        <meta charset="UTF-8">
        <title></title>
        <?php
            include 'includes/scripts.php';      
        ?>
    </head>
  
    <div id="box_wrapper" class="header_style_2">
        <section id="underheader" class="">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 topWrap ">
                        <div class="usermenu_area">
                            <div class="menuUsItem pull-right">
                                <div class="contact_area">
                                    <i class="icon-cellphone67 icon-rounded"></i>Call us on<span class="text-separator"></span>0330 555 0033
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <header id="header" class="">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 topWrap">
                        <div class="logo logo_left">
                            <a class="navbar-brand" href="/">
                                <img class="logo_main" src="/images/logo.png" alt="Gable Healthcare logo">
                                <span class="logo_text">denta<i>lux</i></span>
                            </a>
                        </div>
                        <div class="bottomWrap">
                            <div class="search" title="Open/close search form">
                                <div class="searchForm">
                                    <form role="search" method="get" class="search-form" action="#">
                                        <input type="text" class="searchField" placeholder="" value="" name="s" title="Search for:">
                                        <button type="submit" class="searchSubmit" title="Start search"><span class="icoSearch"></span></button>
                                    </form>
                                </div>
                                <div class="ajaxSearchResults"></div>
                            </div>
                            <a href="#" class="openResponsiveMenu icon-menu"></a>
                            <nav id="mainmenu_wrapper" class="menuTopWrap topMenuStyleLine text-right">
                                <?php include ('utilities/setCurrentPageCSS.php'); ?>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>  
<div id="box_wrapper">
 <!--
    <section id="underheader" class="">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 topWrap ">
                    <div class="usermenu_area">
                        <div class="menuUsItem pull-right">
                            <div class="timetable_area">
                                <span class=""><strong>Mn - St: </strong>8:00am - 9:00pm <strong>Sn: </strong>Closed</span>
                            </div>
                        </div>
                        <div class="menuUsItem pull-left">
                            <div class="contact_area">
                                <i class="icon-cellphone67 icon-rounded"></i>free call<span class="text-separator"></span>0800 133 7566
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <header id="header" class="">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 topWrap">
                    <div class="logo logo_center">
                        <a class="navbar-brand" href="Home">
                            <img class="logo_main" src="/images/logo.png" alt="Gable Healthcare logo">
                            <span class="logo_text">Gable<i>Healthcare</i></span>
                        </a>
                    </div>
                    <div class="bottomWrap">
                        <div class="search" title="Open/close search form">
                            <div class="searchForm">
                                <form role="search" method="get" class="search-form" action="#">
                                    <input type="text" class="searchField" placeholder="" value="" name="s" title="Search for:">
                                    <button type="submit" class="searchSubmit" title="Start search"><span class="icoSearch"></span></button>
                                </form>
                            </div>
                            <div class="ajaxSearchResults"></div>
                        </div>
                        <a href="#" class="openResponsiveMenu icon-menu"></a>
                        <nav id="mainmenu_wrapper" class="menuTopWrap topMenuStyleLine text-center">
                            <?php// include ('utilities/setCurrentPageCSS.php'); ?>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
    -->
    <?php if( $URLResolver->pageFound() && !$URLResolver->isHomePage() ) : ?>
        
<section class="heading breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <h1 class="page_title"><?php echo ucfirst($URLResolver->getParentName()); ?></h1>
            </div>
            <div class="col-xs-6">
                <div class="breadcrumbs text-right">
                    <a class="breadcrumbs_item home" href="/home">Home</a>
                        <?php if(count($URLResolver->getBreadCrump()) > 0): $path = ''; ?>
                            <?php foreach($URLResolver->getBreadCrump() as $crump): ?>
                    
                    <span class="breadcrumbs_delimiter icon-right-open"></span><span class="breadcrumbs_item current">
                                    <a class="breadcrumbs_item home" href="<?php $path = '/'.$crump; echo $path; ?>">
                                <?php echo ucfirst(str_replace('-', ' ', $crump)); ?>
                                    </a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
    <?php endif; ?>