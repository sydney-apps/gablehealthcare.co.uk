<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

    <footer>
        <div class="copyright_area">
            <div class="container">
                <div class="row copy">
                    <div class="col-sm-9 col-xs-12 copyright"><a href="#"> Gable Healthcare</a> &copy; 2012 - <?php date_default_timezone_set('GMT'); echo date('Y'); ?> All Rights Reserved <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a></div>
                    <div class="col-sm-3 col-xs-12 footer_social text-right">
                        <div class="sc_socials">
                                <!-- 
                                <div class="sc_socials_item"><a href="#" target="_blank" class="sc_icon icon-facebook-circled"></a></div>
                                <div class="sc_socials_item"><a href="#" target="_blank" class="sc_icon icon-skype-outline"></a></div>
                                <div class="sc_socials_item"><a href="#" target="_blank" class="sc_icon icon-twitter-circled"></a></div>
                                <div class="sc_socials_item"><a href="#" target="_blank" class="sc_icon icon-linkedin-circled"></a></div>
                            -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- eof #box_wrapper -->

        <div class="preloader">
            <div class="preloader_image"></div>
        </div>

        <!-- libraries -->
        <script src="/js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="/js/vendor/jquery-1.11.3.min.js"></script>
        <script src="/js/vendor/bootstrap.min.js"></script>
        <script src="/js/vendor/jquery.appear.js"></script>
        
        <!-- Custom library -->
        <script src="/js/library.js"></script>

        <!-- superfish menu  -->
        <script src="/js/vendor/superfish.js"></script>
        <!-- page scrolling -->
        <script src="/js/vendor/jquery.ui.totop.js"></script>
        <!-- sliders, filters, carousels -->
        <script src='/js/vendor/jquery.flexslider-min.js'></script>
        <!-- custom scripts -->
        <script src="/js/custom/front.min.js"></script>
        <script src="/js/custom/utils.min.js"></script>
        <script src="/js/custom/shortcodes_init.min.js"></script>
        
        <script src="/js/custom/contact_form.js"></script>

        <script src="/library/osm/OpenLayers.js"></script>
        <script src="/js/os-map.js?v=<?php echo time(); ?>"></script>
</body>
</html>