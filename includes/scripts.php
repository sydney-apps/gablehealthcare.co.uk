
<meta charset="utf-8">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" type="image/png" href="/favicon.ico">
    <link href="/fonts/fontello/css/fontello.css?v=1.0" rel="stylesheet" type="text/css" media="all"/>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
    <link href="/css/flexslider.css" rel="stylesheet" type="text/css">
    <link href="/css/animate.min.css" rel="stylesheet">
    <link href="/css/core.portfolio.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/flexslider.css" type="text/css">
    <link rel="stylesheet" href="css/magnific-popup.min.css">
    <link href="/css/styles.css?v=0.22" rel="stylesheet">
    <link href="/css/responsive.css" rel="stylesheet">
     
    <!--[if lt IE 9]>
        <script src="/js/vendor/html5shiv.min.js"></script>
        <script src="/js/vendor/respond.min.js"></script>
    <![endif]-->