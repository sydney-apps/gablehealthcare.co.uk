/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
    Navigation Controls 
*/

var ONPAGEDIVNAV = function ($stepOne, $navButtonsClass, $positionClass, $nextBtn, $backBtn, $last, $formSubmit) {

	jQuery($stepOne).show();
	var step = jQuery($stepOne).data("position");
	jQuery($backBtn).hide();
	jQuery($formSubmit).hide();

	function HandleNavigation($thisElement) {

		pos = step
		prev = pos - 1;
		next = pos + 1;
		isLast = false;

		/// hide the back button


		if ($thisElement.hasClass('next')) {

			jQuery($positionClass + pos).hide();
			jQuery($positionClass + next).show();
			step++;
			ResetButtons();

			return isLast;

		} else if ($thisElement.hasClass('back')) {

			jQuery($positionClass + pos).hide();
			jQuery($positionClass + prev).show();
			// moveProgressBar(-prev, 'right');
			step--;
			ResetButtons();

			return isLast;

		} else {

			// moveProgressBar(pos, 'left');
			return true;

		}

	}
        
	function ResetButtons() {
		if (step == 0) {
			jQuery($backBtn).hide();
			jQuery($nextBtn).show();
			jQuery($formSubmit).hide();
		}
		else if (step == $last) {
			jQuery($nextBtn).hide();
			jQuery($backBtn).show();
			jQuery($formSubmit).show();
			isLast = true;
		} else {
			jQuery($nextBtn).show();
			jQuery($backBtn).show();
			jQuery($formSubmit).hide();
		}
	}

	jQuery($navButtonsClass).click(function (event) {

		if (!HandleNavigation(jQuery(this))) {
                    event.preventDefault();
                    }

        });

    // SIGNATURE PROGRESS
    moveProgressBar = function ($per, $direction) {

    	console.log("moveProgressBar");
        var getPercent = (($per * 33) / 100);
        var getProgressWrapWidth = jQuery('.progress-wrap').width();
        var progressTotal = getPercent * getProgressWrapWidth;
        var animationLength = 2500;

        // on page load, animate percentage bar to data percentage length
    	// .stop() used to prevent animation queueing

        jQuery('.progress-bar').stop().animate({
            left: progressTotal
        }, animationLength);

    }
};