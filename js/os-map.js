"use strict";

jQuery(document).ready(function () {
    //AIzaSyBgRfHokQNXFKpHlPZl8RwLC1Z3pZkA2zM
    dxmapLoadMap();
});

/*
$("ol.carousel-indicators li").attrchange({
    trackValues: true, // set to true so that the event object is updated with old & new values
    callback: function(evnt) {
        if(evnt.attributeName == "class") { // which attribute you want to watch for changes
            if(evnt.newValue.search(/active/i) == -1) { // "open" is the class name you search for inside "class" attribute
            alert("yes");
        }
    }
}
});*/

var map;
var markers = new OpenLayers.Layer.Markers( "Markers" );

var westbromMap;
var locations = [
	/*
	{
		title: 'Mitcham Office',
		position: {lat: 51.3989476, lng : -0.1791083},
		icon: {
			url: "/images/marker.png"
			//scaledSize: new google.maps.Size(64, 64)
		}

	},
	{
		title: 'Coby Office',
		position: {lat: 52.487517999999994, lng: -0.7005515999999999},
		icon: {
                        url: "/images/marker.png"
			//scaledSize: new google.maps.Size(96, 96)
		}
	},
		
	{
		title: 'West Bromwich',
		position: {lat: 52.5104277, lng: -1.9695836},
		icon: {
                        url: "/images/marker.png"
			//scaledSize: new google.maps.Size(96, 96)
		}
	},
        {
		title: 'Doncaster',
		position: {lat:  53.515, lng:-1.133},
		icon: {
                        url: "/images/marker.png"
			//scaledSize: new google.maps.Size(96, 96)
		}
	},
        {
            title: 'Glocestershire',
            position: {lat: 51.8572102, lng:-2.277532},
            icon:{ 
                url: "/images/marker.png"
            }
        },
        {
            title:"Norwich",
            position: {lat: 52.6402265, lng: 1.2168097},
            icon:{ 
                url: "/images/marker.png"
            }
        },*/
        {
            title:"Northampton",
            position: {lat: 52.277394, lng: -0.86721},
            icon:{ 
                url: "/images/marker.png"
            }
        },
        {
            title:"Walton-On-Thames",
            position: {lat: 51.3908964, lng: -0.4369805},
            icon:{ 
                url: "/images/marker.png"
            }
        }
       
];
    
var styles = [{
    "featureType": "administrative",
    "elementType": "labels.text",
    "stylers": [{"visibility": "off"}]
}, {
    "featureType": "administrative",
    "elementType": "labels.text.fill",
    "stylers": [{"color": "#444444"}]
}, {"featureType": "landscape", "elementType": "all", "stylers": [{"color": "#f2f2f2"}]}, {
    "featureType": "poi",
    "elementType": "all",
    "stylers": [{"visibility": "off"}]
}, {
    "featureType": "road",
    "elementType": "all",
    "stylers": [{"saturation": -100}, {"lightness": 45}]
}, {
    "featureType": "road.highway",
    "elementType": "all",
    "stylers": [{"visibility": "simplified"}]
}, {
    "featureType": "road.arterial",
    "elementType": "labels.icon",
    "stylers": [{"visibility": "off"}]
}, {"featureType": "transit", "elementType": "all", "stylers": [{"visibility": "off"}]}, {
    "featureType": "water",
    "elementType": "all",
    "stylers": [{"color": "#46bcec"}, {"visibility": "on"}]
}, {"featureType": "water", "elementType": "geometry.fill", "stylers": [{"color": "#85d6df"}]}, {
    "featureType": "water",
    "elementType": "labels.text",
    "stylers": [{"visibility": "off"}]
}];

var size = new OpenLayers.Size(25,25);
var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
var icon = new OpenLayers.Icon('/images/marker.png', size, offset);


var pos = 0;
var sizeb = new OpenLayers.Size(50,50);
var offsetb = new OpenLayers.Pixel(-(sizeb.w/2), -sizeb.h);
var iconb = new OpenLayers.Icon('/images/larger-marker.png', sizeb, offsetb);
var fromProjection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
var toProjection   = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection

window.dxmapLoadMap = function () {

    map = new OpenLayers.Map("map");
    var mapnik         = new OpenLayers.Layer.OSM();
    var position       = new OpenLayers.LonLat(-0.1791083, 52.2).transform( fromProjection, toProjection);
    var zoom           = 6; 
        
    map.addLayer(mapnik);
    map.setCenter(position, zoom );
        
    markers = new OpenLayers.Layer.Markers( "Markers" );
    map.addLayer(markers);

    locations.forEach(function(l){
        $(jQuery("ol.carousel-indicators li")).each(function(){
            if(jQuery(this).hasClass('active')){
                    pos = jQuery(this).data('slide-to');
                }
            });
            if(pos === markers.markers.length){
                markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(l.position.lng,l.position.lat).transform(fromProjection, toProjection),iconb.clone()));
            }else{
                markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(l.position.lng,l.position.lat).transform(fromProjection, toProjection),icon.clone()));
            }            
        });
    map.addControl(new OpenLayers.Control.LayerSwitcher());
    //map.zoomToMaxExtent();
    
}
var $div = jQuery('#lione');
var $di = jQuery('#litwo');
var $d = jQuery('#lithree');
var donchaster = jQuery('#lifour');
var norwich = jQuery('#lifive');
var glocestershire = jQuery('#lisix');
var walton = jQuery('#liseven');

var observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
        
        if (mutation.attributeName === "class") {  
            var i= 0;
            /*for(i= 0; i<markers.markers.length; i++){
                markers.markers[i].icon = icon.clone();
                var a = icon.clone();
                markers.markers[i].icon.setSize(a.size);
                markers.markers[i].draw();
            }*/
            var rem = pos % markers.markers.length;
            markers.markers.forEach(function(m){
                console.log(m.icon.url);
               markers.removeMarker(m);
               markers.clearMarkers();
            });

            $(jQuery("ol.carousel-indicators li")).each(function(){

                if(jQuery(this).hasClass('active')){
                        pos = jQuery(this).data('slide-to');
                    }
                });
                                        locations.forEach(function(l){
											console.log(pos +' '+markers.markers.length);
                                                if(pos === markers.markers.length){
                                                    markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(l.position.lng,l.position.lat).transform(fromProjection, toProjection),iconb.clone()));
                                                }else{
                                                    markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(l.position.lng,l.position.lat).transform(fromProjection, toProjection),icon.clone()));
                                                }
                                                    
                                            });

                /*var pos ;
                $(jQuery("ol.carousel-indicators li")).each(function(){
                    if(jQuery(this).hasClass('active')){
                        pos = jQuery(this).data('slide-to');
                    var b = iconb.clone();
                    markers.markers[pos].icon.setSize(b.size);
                    markers.markers[pos].icon.setUrl('/images/larger-marker.png');
                } 
            });
               */       
            
              markers.markers.forEach(function(m){
                markers.drawMarker(m);
            });
            markers.draw();
            pos++;
        }
    });
});

SetObserverAttributes($div);
SetObserverAttributes($d);
SetObserverAttributes($di);
SetObserverAttributes(donchaster);
SetObserverAttributes(norwich);
SetObserverAttributes(glocestershire);

function SetObserverAttributes(elementList) {

	if (elementList.length > 0) {
		observer.observe(elementList[0], {
			attributes: true
		});
	}

}