"use strict";

jQuery(document).ready(function () {
    //AIzaSyBgRfHokQNXFKpHlPZl8RwLC1Z3pZkA2zM
    dxmapLoadMap();
});

/*
$("ol.carousel-indicators li").attrchange({
    trackValues: true, // set to true so that the event object is updated with old & new values
    callback: function(evnt) {
        if(evnt.attributeName == "class") { // which attribute you want to watch for changes
            if(evnt.newValue.search(/active/i) == -1) { // "open" is the class name you search for inside "class" attribute
alert("yes");
            }
        }
    }
});*/

var map;
var markers = new Array();

var westbromMap;
var locations = [
	{
		title: 'Mitcham Office',
		position: {lat: 51.3989476, lng : -0.1791083},
		icon: {
			url: "/images/marker.png"
			//scaledSize: new google.maps.Size(64, 64)
		}

	},
	{
		title: 'Coby Office',
		position: {lat: 52.487517999999994, lng: -0.7005515999999999},
		icon: {
                        url: "/images/marker.png"
			//scaledSize: new google.maps.Size(96, 96)
		}
	},
	{
		title: 'West Bromwich',
		position: {lat: 52.5104277, lng: -1.9695836},
		icon: {
                        url: "/images/marker.png"
			//scaledSize: new google.maps.Size(96, 96)
		}
	},
        {
		title: 'Doncaster',
		position: {lat:  53.515, lng:-1.133},
		icon: {
                        url: "/images/marker.png"
			//scaledSize: new google.maps.Size(96, 96)
		}
	},
        {
            title: 'Glocestershire',
            position: {lat: 51.8572102, lng:-2.277532},
            icon:{ 
                url: "/images/marker.png"
            }
        },
        {
            title:"Norwich",
            position: {lat: 52.6402265, lng: 1.2168097},
            icon:{ 
                url: "/images/marker.png"
            }
        },
        {
            title:"Walton-On-Thames",
            position: {lat: 51.3908964, lng: -0.4369805},
            icon:{ 
                url: "/images/marker.png"
            }
        }
       
];
    
var styles = [{
    "featureType": "administrative",
    "elementType": "labels.text",
    "stylers": [{"visibility": "off"}]
}, {
    "featureType": "administrative",
    "elementType": "labels.text.fill",
    "stylers": [{"color": "#444444"}]
}, {"featureType": "landscape", "elementType": "all", "stylers": [{"color": "#f2f2f2"}]}, {
    "featureType": "poi",
    "elementType": "all",
    "stylers": [{"visibility": "off"}]
}, {
    "featureType": "road",
    "elementType": "all",
    "stylers": [{"saturation": -100}, {"lightness": 45}]
}, {
    "featureType": "road.highway",
    "elementType": "all",
    "stylers": [{"visibility": "simplified"}]
}, {
    "featureType": "road.arterial",
    "elementType": "labels.icon",
    "stylers": [{"visibility": "off"}]
}, {"featureType": "transit", "elementType": "all", "stylers": [{"visibility": "off"}]}, {
    "featureType": "water",
    "elementType": "all",
    "stylers": [{"color": "#46bcec"}, {"visibility": "on"}]
}, {"featureType": "water", "elementType": "geometry.fill", "stylers": [{"color": "#85d6df"}]}, {
    "featureType": "water",
    "elementType": "labels.text",
    "stylers": [{"visibility": "off"}]
}];

window.dxmapLoadMap = function () {
    
    var anotherCenter = new google.maps.LatLng(52.2, -0.1791083);
    var settings = {
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoom: 6,
        draggable: true,
        scrollwheel: true,
        center: anotherCenter,
        styles: styles
    };
    map = new google.maps.Map(document.getElementById('map'), settings);
    var marker;
    
    locations.forEach( function( element ) {
         marker = new google.maps.Marker({
			position: element.position,
			map: map,
			title: element.title,
			icon: element.icon
		});
        markers.push(marker);
	});	

}

var $div = jQuery('#lione');
var $di = jQuery('#litwo');
var $d = jQuery('#lithree');
var donchaster = jQuery('#lifour');
var norwich = jQuery('#lifive');
var glocestershire = jQuery('#lisix');
var walton = jQuery('#liseven');


var observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
        
           if (mutation.attributeName === "class") {     
               var i= 0;
        for(i= 0; i<markers.length; i++){
            markers[i].setIcon('/images/marker.png');
        }
        $(jQuery("ol.carousel-indicators li")).each(function(){

            if(jQuery(this).hasClass('active')){
                var pos = jQuery(this).data('slide-to');
                markers[pos].setIcon('/images/larger-marker.png');
                console.log(pos);
           }        
       });
        }
    });
});

SetObserverAttributes($div);
SetObserverAttributes($d);
SetObserverAttributes($di);
SetObserverAttributes(donchaster);
SetObserverAttributes(norwich);
SetObserverAttributes(glocestershire);

function SetObserverAttributes(elementList) {

	if (elementList.length > 0) {
		observer.observe(elementList[0], {
			attributes: true
		});
	}

}